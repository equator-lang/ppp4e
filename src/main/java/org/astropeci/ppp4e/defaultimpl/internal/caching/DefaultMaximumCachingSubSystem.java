/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.caching;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * A {@link CachingSubSystem} that caches all associations.
 */
public class DefaultMaximumCachingSubSystem<K, V> implements CachingSubSystem<K, V> {

    protected Map<K, V> cache = new HashMap<>();

    @Override
    public boolean isCached(K key) {
        Objects.requireNonNull(key);

        return cache.containsKey(key);
    }

    @Override
    public V getOrThrow(K key) {
        Objects.requireNonNull(key);

        if (isCached(key)) {
            return cache.get(key);
        } else {
            throw new NoSuchElementException("key not in cache");
        }
    }

    @Override
    public void put(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        cache.put(key, value);
    }

    @Override
    public void invalidate(K key) {
        Objects.requireNonNull(key);

        cache.remove(key);
    }

    @Override
    public V access(K key, Supplier<V> supplier) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(supplier);

        if (isCached(key)) {
            return getOrThrow(key);
        } else {
            put(key, supplier.get());
            return getOrThrow(key);
        }
    }
}