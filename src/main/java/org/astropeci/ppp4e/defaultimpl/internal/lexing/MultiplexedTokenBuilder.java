/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Iterator;
import java.util.Set;

/**
 * A {@link TokenBuilder} that manages two underlying {@link TokenBuilder}s.
 */
public class MultiplexedTokenBuilder implements TokenBuilder {

    private final TokenBuilder builder1;
    private final TokenBuilder builder2;

    /**
     * Constructs a new instance with the provided underlying {@link TokenBuilder}s.
     */
    public MultiplexedTokenBuilder(TokenBuilder builder1, TokenBuilder builder2) {
        this.builder1 = builder1;
        this.builder2 = builder2;
    }

    /**
     * Constructs an instance that supports two or more underlying {@link TokenBuilder}s. If the provided sequence has
     * only one {@link TokenBuilder} in it, that {@link TokenBuilder} is returned. If the provided sequence of
     * {@link TokenBuilder}s has two elements, an instance based on those elements is returned. If the provided sequence
     * contains more than two elements, an instance based on the first element and the result of chaining all other
     * elements is returned. If bi elements are present, an {@link IllegalArgumentException} is thrown.
     *
     * @param builders the {@link TokenBuilder}s to chain
     * @return the provided builders chained as one {@link MultiplexedTokenBuilder}
     * @throws IllegalArgumentException if the provided sequence is empty
     */
    public static TokenBuilder chained(Iterable<TokenBuilder> builders) {
        Iterator<TokenBuilder> iter = builders.iterator();

        if (!iter.hasNext()) {
            throw new IllegalArgumentException("no provided builders");
        }

        TokenBuilder builder = iter.next();

        while (iter.hasNext()) {
            builder = new MultiplexedTokenBuilder(builder, iter.next());
        }

        return builder;
    }

    /**
     * @return a set containing both underlying {@link TokenBuilder}s.
     */
    public Set<TokenBuilder> getUnderlying() {
        return Set.of(builder1, builder2);
    }

    /**
     * {@inheritDoc} If the underlying {@link TokenBuilder}s disagree, an {@link IllegalStateException} is thrown.
     *
     * @throws IllegalStateException if the underlying {@link TokenBuilder}s disagree
     */
    @Override
    public boolean isEmpty() {
        if (builder1.isEmpty() != builder2.isEmpty()) {
            throw new IllegalStateException("underlying builders disagree");
        }

        return builder1.isEmpty();
    }

    /**
     * {@inheritDoc} If the underlying {@link TokenBuilder}s disagree, an {@link IllegalStateException} is thrown.
     *
     * @throws IllegalStateException if the underlying {@link TokenBuilder}s disagree
     */
    @Override
    public CharSequence getRaw() {
        if (!builder1.getRaw().toString().equals(builder2.getRaw().toString())) {
            throw new IllegalStateException("underlying builders disagree");
        }

        return builder1.getRaw();
    }

    /**
     * {@inheritDoc} This is equal to the XOR of the result of the {@link TokenBuilder#isValidToken()} methods of both
     * underlying {@link TokenBuilder}s.
     */
    @Override
    public boolean isValidToken() {
        return builder1.isValidToken() ^ builder2.isValidToken();
    }

    /**
     * {@inheritDoc} This is equal to the result of the {@link TokenBuilder#toToken()} method of one of the underlying
     * {@link TokenBuilder}s. If there is an ambiguity, an {@link IllegalStateException} is thrown.
     */
    @Override
    public Token toToken() {
        if (!isValidToken()) {
            throw new IllegalStateException("not a valid token");
        }

        if (builder1.isValidToken()) {
            return builder1.toToken();
        } else {
            return builder2.toToken();
        }
    }

    /**
     * {@inheritDoc} This is equal to the OR of the result of the {@link TokenBuilder#canAppend(char)} methods of both
     * underlying {@link TokenBuilder}s, called with the provided character for this method.
     */
    @Override
    public boolean canAppend(char c) {
        return builder1.canAppend(c) || builder2.canAppend(c);
    }

    /**
     * {@inheritDoc} If both underlying {@link TokenBuilder}s can be have the provided character appended, a new
     * {@link MultiplexedTokenBuilder} is returned with the result of calling {@link TokenBuilder#append(char)} on both
     * underlying {@link TokenBuilder}s, called with the provided character for this method. If only one underlying
     * {@link TokenBuilder} can have the provided character appended, the result of appending the character is returned.
     */
    @Override
    public TokenBuilder append(char c) {
        if (builder1.canAppend(c) && builder2.canAppend(c)) {
            return new MultiplexedTokenBuilder(builder1.append(c), builder2.append(c));
        }

        if (builder1.canAppend(c)) {
            return builder1.append(c);
        }

        if (builder2.canAppend(c)) {
            return builder2.append(c);
        }

        throw new IllegalStateException("cannot append character");
    }
}
