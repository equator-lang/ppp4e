/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.caching;

import java.util.NoSuchElementException;
import java.util.function.Supplier;

/**
 * A system for caching that is used by {@link CachingSystem}.
 *
 * @param <K> the type of the keys of this cache
 * @param <V> the type of the values of this cache
 */
public interface CachingSubSystem<K, V> {

    /**
     * Returns true if the cache contains an entry for the provided key. If this returns true then it is guaranteed that
     * {@link #getOrThrow(Object)} will not throw an exception if it is called before any invocations of
     * {@link #access(Object, Supplier)} or {@link #invalidate(Object)}. If this returns false the
     * {@link #getOrThrow(Object)} method will throw an exception if it is called before any invocations of
     * {@link #put(Object, Object)}, {@link #access(Object, Supplier)} or {@link #invalidate(Object)}.
     *
     * @param key the key that may or may not be cached
     * @return true if the cache contains an entry for the provided key
     */
    boolean isCached(K key);

    /**
     * <p>
     *     Returns the value of the provided key in the cache if it exists, otherwise, this method will throw a
     *     {@link NoSuchElementException}. If the {@link #isCached(Object)} method returns true at the time on calling
     *     this method, then the operation will complete successfully, otherwise it will be unsuccessful.
     * </p>
     * <p>
     *     This method does not affect what associations are cached. This means it is guaranteed that any sequential
     *     invocations of this method with the same key will yield the same result.
     * </p>
     *
     * @param key the key whose value is to be obtained
     * @return the value of the provided key
     * @throws NoSuchElementException if the cache does not have a stored association for the provided key
     */
    V getOrThrow(K key);

    /**
     * Associates a provided value with a provided key. This method can alter the cache's associations in any way,
     * as long as all remain valid.
     *
     * @param key the key to have a value associated with it
     * @param value the value for the key to be associated with
     */
    void put(K key, V value);

    /**
     * Disassociates a key with its value. If the provided key does not have an associated value, this method has no
     * effect. If the provided key does have an associated value, the association is destroyed. Other associations may
     * also be destroyed or created, provided all associations remain valid.
     *
     * @param key the key whose association is invalidated
     */
    void invalidate(K key);

    /**
     * <p>
     *     Accesses the associated value of a key with a default value. If the key has a value associated with it
     *     already, the value is returned and the provided {@link Supplier} is ignored. If there is no association, an
     *     association is created between the provided key and the returned value of the {@link Supplier#get()} method
     *     of the provided {@link Supplier}. The value of this new association is then returned. If an exception is
     *     thrown when calling the {@link Supplier#get()} method of the provided {@link Supplier}, that exception is
     *     thrown from this method. It is worth noting that it is safe to represent expensive operations with the
     *     {@link Supplier}, as its {@link Supplier#get()} method will not be called unless necessary.
     * </p>
     * <p>
     *     It is very likely that this method will alter the what associations are cached.
     * </p>
     *
     * @param key the key to access
     * @param supplier the default value for the provided key
     * @return the value associated with the provided key
     */
    V access(K key, Supplier<V> supplier);
}
