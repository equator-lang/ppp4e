/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.Token;

/**
 * A builder for a {@link Token}.
 */
public interface TokenBuilder {

    /**
     * @return true if this is an empty instance
     */
    boolean isEmpty();

    /**
     * @return the raw characters appended to this instance
     */
    CharSequence getRaw();

    /**
     * Returns true if this instance represents a valid, fully constructed token.
     *
     * @return true if this represents a valid token
     */
    boolean isValidToken();

    /**
     * Returns the token built by this instance. If a valid token cannot be derived from this instance an
     * {@link IllegalStateException} is thrown. This method is guaranteed to return successfully if
     * {@link #isValidToken()} returns true and this method is guaranteed to fail if it returns false.
     *
     * @return the token built by this instance
     * @throws IllegalStateException if a valid token cannot be derived from this instance
     */
    Token toToken();

    /**
     * Returns true if the provided character can be appended to this instance.
     *
     * @param c the character that may or may not be able to be appended
     * @return true if the provided character can be appended to this instance
     */
    boolean canAppend(char c);

    /**
     * Returns a new {@link TokenBuilder} in which the provided character is appended. If the provided character cannot
     * be appended to this instance an {@link IllegalStateException} is thrown. This method is guaranteed to return
     * successfully if {@link #canAppend(char)} returns true for the provided token and this method is guaranteed to
     * fail if it returns false.
     *
     * @param c the character to append
     * @return a {@link TokenBuilder} with the provided character appended
     * @throws IllegalStateException if the provided character cannot be appended to this instance
     */
    TokenBuilder append(char c);

    /**
     * Returns true if this is an anchor. Anchors are fallen back to when an invalid token is encountered.
     * @return true if this is an anchor
     */
    default boolean isAnchor() {
        return false;
    }
}
