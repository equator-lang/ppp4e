/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * A {@link TokenBuilder} that is completely empty.
 */
public class EmptyTokenBuilder implements TokenBuilder {

    private static final Set<Supplier<TokenBuilder>> tokenBuilderConstructors = Set.of(
            StandardIdentifierTokenBuilder::new,
            SymbolicIdentifierTokenBuilder::new
    );

    private static TokenBuilder getInternalBuilder() {
        Stream<TokenBuilder> builders = tokenBuilderConstructors.stream().map(Supplier::get);
        return MultiplexedTokenBuilder.chained(builders::iterator);
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public CharSequence getRaw() {
        return "";
    }

    @Override
    public boolean isValidToken() {
        return false;
    }

    @Override
    public Token toToken() {
        throw new IllegalStateException("not valid token");
    }

    @Override
    public boolean canAppend(char c) {
        return getInternalBuilder().canAppend(c);
    }

    @Override
    public TokenBuilder append(char c) {
        return getInternalBuilder().append(c);
    }

    @Override
    public boolean isAnchor() {
        return true;
    }
}
