/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.IdentifierToken;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A {@link TokenBuilder} for {@link IdentifierToken}s that are symbolic identifiers.
 */
public class SymbolicIdentifierTokenBuilder extends AbstractTokenBuilder {

    private static final Set<Integer> WHITELIST = Stream.of(
            '~',
            '!',
            '$',
            '%',
            '^',
            '&',
            '*',
            '-',
            '=',
            '+',
            '\\',
            '|',
            ':',
            '<',
            '>',
            '/',
            '?',
            '_'
    ).map(c -> (int) c).collect(Collectors.toSet());

    public SymbolicIdentifierTokenBuilder() {
        super();
    }

    private SymbolicIdentifierTokenBuilder(String content) {
        super(content);
    }

    @Override
    public boolean isValidToken() {
        return !isEmpty();
    }

    @Override
    public IdentifierToken toToken() {
        if (isValidToken()) {
            return new IdentifierTokenImpl(getRaw(), true);
        } else {
            throw new IllegalStateException("not a valid token");
        }
    }

    @Override
    public boolean canAppend(char c) {
        if (c == '.') {
            return isEmpty();
        }

        return WHITELIST.contains((int) c);
    }

    @Override
    public SymbolicIdentifierTokenBuilder append(char c) {
        return (SymbolicIdentifierTokenBuilder) super.append(c);
    }

    @Override
    protected SymbolicIdentifierTokenBuilder getBuilderWithNewContent(String content) {
        return new SymbolicIdentifierTokenBuilder(content);
    }

    public static boolean isValidSymbolicIdentifier(CharSequence identifier) {
        boolean allCharsAreValid = identifier.codePoints().allMatch(c ->
                WHITELIST.contains(c) || c == '.'
        );

        boolean outOfBoundsPeriods = identifier.toString().lastIndexOf('.') > 0;

        return allCharsAreValid && !outOfBoundsPeriods && identifier.length() > 0;
    }
}
