/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.caching;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A {@link CachingSubSystem} that tracks the usage of keys and caches the most used ones. For keys that are
 * tracked, but not cached, a configurable hash of the key will be stored. This means that this class will perform
 * badly if many keys with high hash collision are accessed.
 */
public class DefaultNormalCachingSubSystem<K, V> implements CachingSubSystem<K, V> {

    protected Map<K, ValueAndTimesAccessed> cache = new HashMap<>();

    /** A mapping from hashes to amount of times accessed. */
    protected Map<Integer, Integer> cacheCandidates = new HashMap<>();

    protected final int storedAssociations;
    /** A function to generate hashes for keys. */
    protected final Function<K, Integer> hashFunction;

    /**
     * Constructs a new instance with 256 stored associations and a hash function that delegates to
     * {@link Object#hashCode()}. This constructor behaves as if by calling
     * {@link DefaultNormalCachingSubSystem#DefaultNormalCachingSubSystem(int)} with an argument of 256.
     */
    public DefaultNormalCachingSubSystem() {
        this(256);
    }

    /**
     * Constructs a new instance with the provided amount of stored associations and a hash function that delegates
     * to {@link Object#hashCode()}. This constructor behaves as if by calling
     * {@link DefaultNormalCachingSubSystem#DefaultNormalCachingSubSystem(int, Function)} with the provided amount
     * of stored associations and a hash function implementation that delegates to {@link Object#hashCode()}.
     *
     * @param storedAssociations the amount of associations to keep cached
     */
    public DefaultNormalCachingSubSystem(int storedAssociations) {
        this(storedAssociations, Object::hashCode);
    }

    /**
     * Constructs a new instance with the provided amount of stored associations and hash function. The amount of
     * stored associations determines the maximum amount of associations that are to be
     * cached. The provided hash function is used to hash keys in order to store information about keys without
     * incurring the overhead of actually storing the key. If the hash function has a high collision, the cache
     * will not be optimal.
     *
     * @param storedAssociations the maximum amount of associations to cache
     * @param hashFunction a function to hash keys
     */
    public DefaultNormalCachingSubSystem(int storedAssociations, Function<K, Integer> hashFunction) {
        Objects.requireNonNull(hashFunction);

        this.storedAssociations = storedAssociations;
        this.hashFunction = hashFunction;
    }

    /**
     * A value and the amount of times it has been accessed.
     */
    protected final class ValueAndTimesAccessed {

        public V value;
        public int timesAccessed;

        public ValueAndTimesAccessed(V value, int timesAccessed) {
            Objects.requireNonNull(value);

            this.value = value;
            this.timesAccessed = timesAccessed;
        }
    }

    @Override
    public boolean isCached(K key) {
        Objects.requireNonNull(key);

        return cache.containsKey(key);
    }

    @Override
    public V getOrThrow(K key) {
        Objects.requireNonNull(key);

        if (isCached(key)) {
            return cache.get(key).value;
        } else {
            throw new NoSuchElementException("key not in cache");
        }
    }

    @Override
    public void put(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        if (isCached(key)) {
            cache.get(key).value = value;
        }
    }

    @Override
    public void invalidate(K key) {
        Objects.requireNonNull(key);

        cache.remove(key);
        cacheCandidates.remove(hashFunction.apply(key));
    }
    /**
     * Hints to the instance that a particular key was used. This will be taken as evidence that this particular key
     * is likely to be needed again in the future. The more this method is called with a particular key, the more
     * likely it is that key will be cached. If the instance decides that this method should cause the key to be
     * cached, but it is not already, the provided supplier can be used to provide a value for
     *
     * @param key the key that was used
     * @param supplier a supplier that provides the value of the key if needed
     */
    public void addInfluence(K key, Supplier<V> supplier) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(supplier);

        if (isCached(key)) {
            addInfluenceToCachedAssociation(key);
        } else {
            addInfluenceToUncachedAssociation(key, supplier);
        }
    }

    private void addInfluenceToCachedAssociation(K key) {
        assert key != null;

        cache.get(key).timesAccessed++;
    }

    private void addInfluenceToUncachedAssociation(K key, Supplier<V> supplier) {
        assert key != null;
        assert supplier != null;

        int hash = hashFunction.apply(key);
        int timesAccessed = (cacheCandidates.get(hash) == null ? 0 : cacheCandidates.get(hash)) + 1;
        cacheCandidates.put(hash, timesAccessed);

        if (cache.size() < storedAssociations) {
            promoteAssociation(hash, key, supplier.get());
        } else {
            Map.Entry<K, ValueAndTimesAccessed> leastAccessedCachedAssociation =
                    getLeastAccessedCachedAssociation();
            if (leastAccessedCachedAssociation.getValue().timesAccessed < timesAccessed) {
                demoteAssociation(
                        hashFunction.apply(leastAccessedCachedAssociation.getKey()),
                        leastAccessedCachedAssociation.getKey());
                promoteAssociation(hash, key, supplier.get());
            }
        }
    }

    private void promoteAssociation(int keyHash, K key, V value) {
        assert key != null;
        assert value != null;

        int timesAccessed = cacheCandidates.remove(keyHash);
        cache.put(key, new ValueAndTimesAccessed(value, timesAccessed));
    }

    private void demoteAssociation(int keyHash, K key) {
        assert key != null;

        int timesAccessed = cache.remove(key).timesAccessed;
        cacheCandidates.put(keyHash, timesAccessed);
    }

    private Map.Entry<K, ValueAndTimesAccessed> getLeastAccessedCachedAssociation() {
        Map.Entry<K, ValueAndTimesAccessed> leastAccessedCacheItem = null;
        for (Map.Entry<K, ValueAndTimesAccessed> entry : cache.entrySet()) {
            if (leastAccessedCacheItem == null ||
                    entry.getValue().timesAccessed < leastAccessedCacheItem.getValue().timesAccessed) {
                leastAccessedCacheItem = entry;
            }
        }

        if (leastAccessedCacheItem == null) {
            throw new NoSuchElementException("no elements in cache");
        }

        return leastAccessedCacheItem;
    }

    /**
     * {@inheritDoc} This method calls {@link #addInfluence(Object, Supplier)} and is therefore a good fit for
     * general accessing of the cache.
     */
    @Override
    public V access(K key, Supplier<V> supplier) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(supplier);

        if (isCached(key)) {
            addInfluence(key, () -> { throw new AssertionError(); });
            return getOrThrow(key);
        } else {
            V suppliedValue = supplier.get();
            addInfluence(key, () -> suppliedValue);
            return suppliedValue;
        }
    }
}