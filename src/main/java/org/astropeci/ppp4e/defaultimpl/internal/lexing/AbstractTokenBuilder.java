/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import java.util.Objects;

/**
 * A partial implementation of {@link TokenBuilder} that provides some utilities.
 */
public abstract class AbstractTokenBuilder implements TokenBuilder {

    private final String content;

    /**
     * Constructs an empty instance.
     */
    public AbstractTokenBuilder() {
        this("");
    }

    /**
     * Constructs an instance with the provided content.
     *
     * @param content the content to fill the instance with
     */
    public AbstractTokenBuilder(String content) {
        Objects.requireNonNull(content);

        this.content = content;
    }

    @Override
    public boolean isEmpty() {
        return content.length() == 0;
    }

    @Override
    public CharSequence getRaw() {
        return content;
    }

    @Override
    public TokenBuilder append(char c) {
        if (canAppend(c)) {
            return getBuilderWithNewContent(content + c);
        } else {
            throw new IllegalStateException("cannot append character");
        }
    }

    /**
     * Returns a {@link TokenBuilder} that contains the provided content. The result of this method is returned from
     * {@link #append(char)} when it succeeds.
     *
     * @param content the content for the new {@link TokenBuilder}
     * @return a {@link TokenBuilder} that contains the provided content
     */
    protected abstract TokenBuilder getBuilderWithNewContent(String content);
}
