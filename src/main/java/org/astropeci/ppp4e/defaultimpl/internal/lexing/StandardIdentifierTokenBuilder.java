/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.IdentifierToken;

import java.util.Objects;
import java.util.Optional;

/**
 * A {@link TokenBuilder} for {@link IdentifierToken}s that are standard identifiers.
 */
public class StandardIdentifierTokenBuilder extends AbstractTokenBuilder {

    private static final char ASCII_ZERO = 48;
    private static final char ASCII_NINE = 57;

    /**
     * Constructs an empty instance.
     */
    public StandardIdentifierTokenBuilder() {
        super();
    }

    private StandardIdentifierTokenBuilder(String content) {
        super(content);
    }

    @Override
    public boolean isValidToken() {
        return
                // No trailing high surrogate and not empty.
                lastChar().map(last -> !Character.isHighSurrogate(last)).orElse(false) &&
                        !SymbolicIdentifierTokenBuilder.isValidSymbolicIdentifier(getRaw());
    }

    @Override
    public IdentifierToken toToken() {
        if (isValidToken()) {
            return new IdentifierTokenImpl(getRaw(), false);
        } else {
            throw new IllegalStateException("not a valid token");
        }
    }

    @Override
    public boolean canAppend(char c) {
        if (Character.isHighSurrogate(c)) {
            return lastChar().map(
                    last -> !Character.isHighSurrogate(last))
                    .orElse(true);
        }

        if (Character.isLowSurrogate(c)) {
            return lastChar().map(
                    last -> Character.isHighSurrogate(last) &&
                            Character.isSurrogatePair(last, c) &&
                            isStandardIdentifierChar(Character.toCodePoint(last, c)))
                    .orElse(false);
        } else if (lastChar().map(Character::isHighSurrogate).orElse(false)) {
            return false;
        }

        return isStandardIdentifierChar(c) || isEmpty() && c == '.';
    }

    @Override
    public StandardIdentifierTokenBuilder append(char c) {
        return (StandardIdentifierTokenBuilder) super.append(c);
    }

    @Override
    protected TokenBuilder getBuilderWithNewContent(String content) {
        return new StandardIdentifierTokenBuilder(content);
    }

    /**
     * Returns true if the provided char sequence is a valid standard identifier in Equator.
     *
     * @param identifier the char sequence to test
     * @return true if the provided char sequence is a valid standard identifier
     */
    public static boolean isValidStandardIdentifier(CharSequence identifier) {
        Objects.requireNonNull(identifier);

        boolean allCharsAreValid = identifier.codePoints().allMatch(c -> isStandardIdentifierChar(c) || c == '.');
        boolean outOfBoundsPeriods = identifier.toString().lastIndexOf('.') > 0;
        boolean ambiguous = SymbolicIdentifierTokenBuilder.isValidSymbolicIdentifier(identifier);

        return allCharsAreValid && !outOfBoundsPeriods && !ambiguous && identifier.length() > 0;
    }

    private static boolean isStandardIdentifierChar(int codePoint) {
        return Character.isLetter(codePoint) ||
                codePoint >= ASCII_ZERO && codePoint <= ASCII_NINE ||
                codePoint == '_';
    }

    private Optional<Character> lastChar() {
        if (isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(getRaw().charAt(getRaw().length() - 1));
        }
    }
}
