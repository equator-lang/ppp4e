/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * A stream of tokens that can have characters appended to it.
 */
public class TokenBuildingStream {

    private List<Token> tokens = new ArrayList<>();
    private TokenBuilder builder = new EmptyTokenBuilder();
    private TokenBuilder anchor = builder;

    /**
     * Returns true if the provided character can be appended to this instance.
     *
     * @param c the character that may or may not be able to be appended
     * @return true if the provided character can be appended to this instance
     */
    public boolean canAppend(char c) {
        return builder.canAppend(c) || (builder.isValidToken() && new EmptyTokenBuilder().canAppend(c));
    }

    /**
     * Appends the provided character. If the provided character cannot be appended to this instance an
     * {@link IllegalStateException} is thrown. This method is guaranteed to return successfully if
     * {@link #canAppend(char)} returns true for the provided token and this method is guaranteed to fail if it returns
     * false.
     *
     * @param c the character to append
     * @throws IllegalStateException if the provided character cannot be appended to this instance
     */
    public void append(char c) {
        if (builder.canAppend(c)) {
            builder = builder.append(c);
        } else {
            tokens.add(builder.toToken());
            builder = new EmptyTokenBuilder().append(c);
        }

        if (builder.isAnchor()) {
            anchor = builder;
        }
    }

    /**
     * Returns true if this instance represents a sequence of valid, fully constructed tokens.
     *
     * @return true if this represents a valid sequence of tokens
     */
    public boolean isValidStream() {
        return builder.isValidToken() || builder.isEmpty();
    }

    public Iterable<Token> toTokens() {
        if (!isValidStream()) {
            throw new IllegalStateException("not valid stream");
        }

        List<Token> result = new ArrayList<>(tokens.size());
        result.addAll(tokens);
        if (!builder.isEmpty()) {
            result.add(builder.toToken());
        }

        return result;
    }

    public void reset() {
        builder = anchor;
    }
}
