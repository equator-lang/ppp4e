/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.IdentifierToken;

import java.util.Objects;

/**
 * An implementation of {@link org.astropeci.ppp4e.parsing.lexing.token.IdentifierToken} that supplies {@link #getRaw()}
 * and {@link #isSymbolic()} through constructor parameters.
 */
public final class IdentifierTokenImpl extends IdentifierToken {

    private final CharSequence raw;
    private final boolean isSymbolic;

    /**
     * Constructs an instance with the provided values for {@link #getRaw()} and {@link #isSymbolic()}.
     *
     * @param raw the value for {@link #getRaw()}
     * @param isSymbolic the value for {@link #isSymbolic()}
     */
    public IdentifierTokenImpl(CharSequence raw, boolean isSymbolic) {
        this.raw = raw;
        this.isSymbolic = isSymbolic;
    }

    @Override
    public boolean isSymbolic() {
        return isSymbolic;
    }

    @Override
    public CharSequence getRaw() {
        return raw;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IdentifierToken) {
            IdentifierToken other = (IdentifierToken) obj;
            return getRaw().equals(other.getRaw()) && isSymbolic() == other.isSymbolic();
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRaw(), isSymbolic());
    }
}
