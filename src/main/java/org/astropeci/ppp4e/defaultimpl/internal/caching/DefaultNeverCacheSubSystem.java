/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.caching;

import java.util.NoSuchElementException;
import java.util.function.Supplier;

public class DefaultNeverCacheSubSystem<K, V> implements CachingSubSystem<K, V> {

    /**
     * @return false
     */
    @Override
    public boolean isCached(K key) {
        return false;
    }

    /**
     * @return this method never returns normally
     * @throws NoSuchElementException always
     */
    @Override
    public V getOrThrow(K key) {
        throw new NoSuchElementException("key not in cache");
    }

    /**
     * {@inheritDoc} This method has no effect on this instance.
     */
    @Override
    public void put(K key, V value) { }

    /**
     * {@inheritDoc} This method has no effect on this instance.
     */
    @Override
    public void invalidate(K key) { }

    /**
     * {@inheritDoc} This instance will always call the {@link Supplier#get()} method of the provided supplier.
     */
    @Override
    public V access(K key, Supplier<V> supplier) {
        return supplier.get();
    }
}