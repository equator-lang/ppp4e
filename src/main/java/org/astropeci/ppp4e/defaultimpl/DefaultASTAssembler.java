/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.defaultimpl.internal.caching.CachingSystem;
import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.logging.ParseMessage;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.ast.ASTAssembler;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * A default implementation of {@link ASTAssembler}.
 */
public class DefaultASTAssembler implements ASTAssembler {

    protected final CachingSystem<Iterable<Token>, AssembleResult> cache =
            new CachingSystem<>(CacheHint.SPARSE_CACHING);

    protected final class AssembleResult {

        public AST ast;
        public Iterable<ParseMessage> parseMessages;

        public AssembleResult(AST ast, Iterable<ParseMessage> parseMessages) {
            this.ast = ast;
            this.parseMessages = parseMessages;
        }
    }

    @Override
    public Optional<AST> assemble(PPP4eLogger logger, Iterable<Token> tokens) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(tokens);

        AssembleResult result = cache.access(tokens, () -> {
            List<ParseMessage> parseMessages = new ArrayList<>();
            PPP4eLogger storeLogger = parseMessages::add;

            AST ast = assembleUncached(storeLogger, tokens).orElse(null);
            return new AssembleResult(ast, parseMessages);
        });

        result.parseMessages.forEach(logger::log);
        return Optional.ofNullable(result.ast);
    }

    private Optional<AST> assembleUncached(PPP4eLogger logger, Iterable<Token> tokens) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public void applyCacheHint(CacheHint hint) {

    }
}
