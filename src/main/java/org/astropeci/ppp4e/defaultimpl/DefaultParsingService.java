/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.loading.ParsingService;
import org.astropeci.ppp4e.parsing.CacheHint;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;

/**
 * A default implementation of {@link ParsingService}.
 */
public class DefaultParsingService implements ParsingService {

    /**
     * @return {@link Integer#MIN_VALUE}
     */
    @Override
    public int getPriority() {
        return Integer.MIN_VALUE;
    }

    @Override
    public DefaultParser createParser(CacheHint cacheHint) {
        Objects.requireNonNull(cacheHint);

        DefaultParser parser = new DefaultParser();
        parser.applyCacheHint(cacheHint);
        return parser;
    }

    @Override
    public DefaultParsingContext createParsingContext(CharSequence program, CacheHint cacheHint) {
        Objects.requireNonNull(program);
        Objects.requireNonNull(cacheHint);

        DefaultParsingContext context = new DefaultParsingContext(program);
        context.applyCacheHint(cacheHint);
        return context;
    }

    @Override
    public DefaultURLParsingContext createParsingContext(URL url, Charset charset, CacheHint cacheHint) {
        Objects.requireNonNull(url);
        Objects.requireNonNull(charset);
        Objects.requireNonNull(cacheHint);

        DefaultURLParsingContext context = new DefaultURLParsingContext(url, charset);
        context.applyCacheHint(cacheHint);
        return context;
    }
}
