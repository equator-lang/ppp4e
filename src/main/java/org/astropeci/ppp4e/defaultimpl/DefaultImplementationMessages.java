/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.apache.commons.lang3.text.StrSubstitutor;

import java.util.Map;

/**
 * A set of global messages used in conjunction with {@link org.astropeci.ppp4e.logging.PPP4eLogger}s.
 */
public class DefaultImplementationMessages {

    public static final Message unexpectedToken = new Message("unexpected token (%(char)) at line %(line):%(column)");

    /**
     * A string message that can be formatted with replacements.
     */
    public static class Message {

        /**
         * The raw source for the message.
         */
        public String raw;

        public Message(String message) {
            raw = message;
        }

        /**
         * @return the message formatted with the provided replacements
         */
        public String get(Map<String, String> replacements) {
            return new StrSubstitutor(replacements, "%(", ")").replace(raw);
        }
    }
}
