/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.ParsingContext;
import org.astropeci.ppp4e.parsing.URLParsingContext;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Optional;

/**
 * A default implementation of {@link URLParsingContext} that wraps an implementation of {@link ParsingContext}.
 */
public class DefaultURLParsingContext implements URLParsingContext {

    protected URL url;
    protected Charset charset;
    protected final ParsingContext wrapped;

    /**
     * Constructs an instance tied to the provided {@link URL}. This instance interprets the source code located at the
     * {@link URL} in the provided {@link Charset}. The instance wraps the implementation of the provided
     * {@link ParsingContext}.
     *
     * @param url the {@link URL} this instance is tied to
     * @param charset the {@link Charset} to interpret source code with
     * @param wrapped the {@link ParsingContext} this instance wraps
     */
    public DefaultURLParsingContext(URL url, Charset charset, ParsingContext wrapped) {
        Objects.requireNonNull(url);
        Objects.requireNonNull(charset);
        Objects.requireNonNull(wrapped);

        this.url = url;
        this.charset = charset;
        this.wrapped = wrapped;
    }

    /**
     * Constructs an instance tied to the provided {@link URL}. The instance wraps an instance of
     * {@link DefaultParsingContext}. This instance interprets the source code located at the {@link URL} in the
     * provided {@link Charset}. This constructor behaves as if it delegates to
     * {@link #DefaultURLParsingContext(URL, Charset, ParsingContext)} with a newly constructed instance of
     * {@link DefaultParsingContext} with empty program source.
     *
     * @param url the {@link URL} this instance is tied to
     * @param charset the {@link Charset} to interpret source code with
     */
    public DefaultURLParsingContext(URL url, Charset charset) {
        this(url, charset, new DefaultParsingContext(""));
    }

    /**
     * @return the {@link ParsingContext} this instance wraps
     */
    public ParsingContext getWrapped() {
        return wrapped;
    }

    /**
     * Calls {@link #getContentFromCurrentURL()} in order to obtain the new source code. This method replaces the source
     * code by calling {@link #replaceSource(int, int, CharSequence)} with a start of {@code 0} and a length equal to
     * the result of {@link #getLength()}.
     */
    @Override
    public void resetFromURL() throws IOException {
        replaceSource(0, getLength(), getContentFromCurrentURL());
    }

    /**
     * Obtains the source code located at {@link #getURL()}.
     *
     * @return the source code at the result of {@link #getURL()}
     * @throws IOException if an underlying error occurs reading from the {@link URL}
     */
    protected CharSequence getContentFromCurrentURL() throws IOException {
        URL url = getURL();
        InputStreamReader reader = new InputStreamReader(url.openStream(), getCharset());

        StringBuilder builder = new StringBuilder();

        final int bufferCapacity = 2048;
        char[] buffer = new char[bufferCapacity];
        for (int amountRead; (amountRead = reader.read(buffer)) != -1;) {
            builder.append(buffer, 0, amountRead);
            buffer = new char[bufferCapacity];
        }

        return builder;
    }

    @Override
    public URL getURL() {
        return url;
    }

    @Override
    public void setURL(URL url) {
        Objects.requireNonNull(url);

        this.url = url;
    }

    @Override
    public Charset getCharset() {
        return charset;
    }

    @Override
    public void setCharset(Charset charset) {
        Objects.requireNonNull(charset);

        this.charset = charset;
    }

    /**
     * Delegates to the {@link ParsingContext#getLength()} method of {@link #getWrapped()}.
     */
    @Override
    public int getLength() {
        return wrapped.getLength();
    }

    /**
     * Delegates to the {@link ParsingContext#getRaw()} method of {@link #getWrapped()}.
     */
    @Override
    public CharSequence getRaw() {
        return wrapped.getRaw();
    }

    /**
     * Delegates to the {@link ParsingContext#replaceSource(int, int, CharSequence)} method of {@link #getWrapped()}.
     */
    @Override
    public void replaceSource(int start, int length, CharSequence replacement) {
        Objects.requireNonNull(replacement);

        wrapped.replaceSource(start, length, replacement);
    }

    /**
     * Delegates to the {@link ParsingContext#parse(PPP4eLogger)} method of {@link #getWrapped()}.
     */
    @Override
    public Optional<AST> parse(PPP4eLogger logger) {
        Objects.requireNonNull(logger);

        return wrapped.parse(logger);
    }

    /**
     * Delegates to the {@link ParsingContext#lex(PPP4eLogger)} method of {@link #getWrapped()}.
     */
    @Override
    public Optional<Iterable<Token>> lex(PPP4eLogger logger) {
        Objects.requireNonNull(logger);

        return wrapped.lex(logger);
    }

    /**
     * Delegates to the {@link ParsingContext#applyCacheHint(CacheHint)} method of {@link #getWrapped()}.
     */
    @Override
    public void applyCacheHint(CacheHint hint) {
        Objects.requireNonNull(hint);

        wrapped.applyCacheHint(hint);
    }
}
