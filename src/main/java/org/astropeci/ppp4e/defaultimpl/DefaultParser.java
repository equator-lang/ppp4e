/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.Parser;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.ast.ASTAssembler;
import org.astropeci.ppp4e.parsing.lexing.Lexer;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Objects;
import java.util.Optional;

/**
 * A default implementation of {@link Parser} that is backed by an underlying {@link DefaultLexer} and
 * {@link ASTAssembler}.
 */
public class DefaultParser implements Parser {

    protected final Lexer lexer;
    protected final ASTAssembler assembler;

    /**
     * Constructs a new instance backed by the provided {@link Lexer} and {@link ASTAssembler}. This construct invokes
     * the {@link org.astropeci.ppp4e.parsing.CacheHintReceiver#applyCacheHint(CacheHint)} method of the provided
     * {@link Lexer} and {@link ASTAssembler} with an argument of {@link CacheHint#SPARSE_CACHING}.
     *
     * @param lexer the {@link Lexer} this is backed by
     * @param assembler the {@link ASTAssembler} this is backed by
     */
    public DefaultParser(Lexer lexer, ASTAssembler assembler) {
        this.lexer = lexer;
        this.assembler = assembler;

        applyCacheHint(CacheHint.SPARSE_CACHING);
    }

    /**
     * Constructs a new instance backed by a {@link DefaultLexer} and {@link DefaultASTAssembler}.
     */
    public DefaultParser() {
        this(new DefaultLexer(), new DefaultASTAssembler());
    }

    @Override
    public Optional<AST> parse(PPP4eLogger logger, CharSequence program) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(program);

        return lex(logger, program).flatMap(tokens -> assemble(logger, tokens));
    }

    @Override
    public Optional<Iterable<Token>> lex(PPP4eLogger logger, CharSequence program) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(program);

        return lexer.lex(logger, program);
    }

    @Override
    public Optional<AST> assemble(PPP4eLogger logger, Iterable<Token> tokens) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(tokens);

        return assembler.assemble(logger, tokens);
    }

    /**
     * {@inheritDoc} This method simply delegates to
     * {@link org.astropeci.ppp4e.parsing.CacheHintReceiver#applyCacheHint(CacheHint)} method of
     * {@link #getLexer()} and {@link #getAssembler()}
     */
    @Override
    public void applyCacheHint(CacheHint hint) {
        Objects.requireNonNull(hint);

        assembler.applyCacheHint(hint);
        lexer.applyCacheHint(hint);
    }

    /**
     * @return the {@link Lexer} that is used to back this instance
     */
    public Lexer getLexer() {
        return lexer;
    }

    /**
     *
     * @return the {@link ASTAssembler} that is used to back this instance
     */
    public ASTAssembler getAssembler() {
        return assembler;
    }
}
