/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.Parser;
import org.astropeci.ppp4e.parsing.ParsingContext;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Objects;
import java.util.Optional;

/**
 * A default implementation of {@link ParsingContext} backed by a {@link Parser}.
 */
public class DefaultParsingContext implements ParsingContext {

    protected String programSource;
    protected final Parser parser;

    /**
     * Constructs a parsing context with empty source code and a backing {@link DefaultParser}.
     */
    public DefaultParsingContext() {
        this("");
    }

    /**
     * Constructs a parsing context with the provided initial source code and a backing {@link DefaultParser}.
     *
     * @param program the initial source code
     */
    public DefaultParsingContext(CharSequence program) {
        this(program, new DefaultParser());
    }

    /**
     * Constructs a parsing context with the provided initial source code and provided {@link Parser}. This constructor
     * invokes the {@link org.astropeci.ppp4e.parsing.CacheHintReceiver#applyCacheHint(CacheHint)} method of the
     * provided {@link Parser} with an argument of {@link CacheHint#NORMAL_CACHING}.
     *
     * @param program the initial source code
     * @param parser the {@link Parser} this is backed by
     */
    public DefaultParsingContext(CharSequence program, Parser parser) {
        Objects.requireNonNull(program);
        Objects.requireNonNull(parser);

        programSource = program.toString();
        this.parser = parser;
        parser.applyCacheHint(CacheHint.NORMAL_CACHING);
    }

    @Override
    public int getLength() {
        return programSource.length();
    }

    @Override
    public CharSequence getRaw() {
        return programSource;
    }

    @Override
    public void replaceSource(int start, int length, CharSequence replacement) {
        Objects.requireNonNull(replacement);

        if (start < 0 || length < 0 || start + length > programSource.length()) {
            throw new IndexOutOfBoundsException();
        }

        // Special case for equal source and replacement.
        if (length == replacement.length()) {
            if (programSource.substring(start, start + length).contentEquals(replacement)) {
                return;
            }
        }

        int uselessCharsAtStartOfReplacement =
                getUselessCharsAtStartOfReplacement(start, length, programSource, replacement);

        // Replace start and replacement with more optimal versions.
        start = start + uselessCharsAtStartOfReplacement;
        replacement = replacement.subSequence(uselessCharsAtStartOfReplacement, replacement.length());

        int uselessCharsAtEndOfReplacement =
                getUselessCharsAtEndOfReplacement(start, length, programSource, replacement);

        // Replace length and replacement with more optimal versions.
        length = length - uselessCharsAtStartOfReplacement;
        replacement = replacement.subSequence(0, replacement.length() - uselessCharsAtEndOfReplacement);

        replaceSourceRaw(start, length, replacement);
    }

    private int getUselessCharsAtStartOfReplacement(int start, int length, CharSequence source,
                                                    CharSequence replacement) {
        for (int uselessChars = 0;; uselessChars++) {
            int sourceCharIndex = start + uselessChars;
            int replacementCharIndex = uselessChars;

            boolean outOfBounds = sourceCharIndex >= source.length() || replacementCharIndex >= replacement.length();

            if (outOfBounds || source.charAt(sourceCharIndex) != replacement.charAt(replacementCharIndex)) {
                return uselessChars;
            }
        }
    }

    private int getUselessCharsAtEndOfReplacement(int start, int length, CharSequence source,
                                                  CharSequence replacement) {
        for (int uselessChars = 0;; uselessChars++) {
            int sourceCharIndex = start + length - 1 - uselessChars;
            int replacementCharIndex = replacement.length() - 1 - uselessChars;

            boolean outOfBounds = sourceCharIndex < start || replacementCharIndex < 0;

            if (outOfBounds || source.charAt(start + length - 1 - uselessChars) !=
                    replacement.charAt(replacement.length() - 1 - uselessChars)) {
                return uselessChars;
            }
        }
    }

    private void replaceSourceRaw(int start, int length, CharSequence replacement) {
        String head = programSource.substring(0, start);
        String tail = programSource.substring(start + length);

        // As efficient as using a StringBuilder.
        programSource = head + replacement + tail;
    }

    @Override
    public Optional<AST> parse(PPP4eLogger logger) {
        Objects.requireNonNull(logger);

        return parser.parse(logger, programSource);
    }

    @Override
    public Optional<Iterable<Token>> lex(PPP4eLogger logger) {
        Objects.requireNonNull(logger);

        return parser.lex(logger, programSource);
    }

    /**
     * {@inheritDoc} This method delegates to the
     * {@link org.astropeci.ppp4e.parsing.CacheHintReceiver#applyCacheHint(CacheHint)} method of {@link #getParser()}.
     */
    @Override
    public void applyCacheHint(CacheHint hint) {
        Objects.requireNonNull(hint);

        parser.applyCacheHint(hint);
    }

    /**
     * @return the parser this instance is backed by
     */
    public Parser getParser() {
        return parser;
    }
}
