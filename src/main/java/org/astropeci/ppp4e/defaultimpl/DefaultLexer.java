/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.defaultimpl.internal.caching.CachingSystem;
import org.astropeci.ppp4e.defaultimpl.internal.lexing.TokenBuildingStream;
import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.logging.ParseMessage;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.lexing.Lexer;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.*;

/**
 * A default implementation of {@link Lexer}.
 */
public class DefaultLexer implements Lexer {

    protected final CachingSystem<CharSequence, LexResult> cache = new CachingSystem<>(CacheHint.SPARSE_CACHING);

    protected final class LexResult {

        public Iterable<Token> tokens;
        public Iterable<ParseMessage> parseMessages;

        public LexResult(Iterable<Token> tokens, Iterable<ParseMessage> parseMessages) {
            this.tokens = tokens;
            this.parseMessages = parseMessages;
        }
    }

    @Override
    public Optional<Iterable<Token>> lex(PPP4eLogger logger, CharSequence program) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(program);

        LexResult result = cache.access(program, () -> {
            List<ParseMessage> parseMessages = new ArrayList<>();
            PPP4eLogger storeLogger = parseMessages::add;

            Iterable<Token> tokens = lexUncached(storeLogger, program).orElse(null);
            return new LexResult(tokens, parseMessages);
        });

        result.parseMessages.forEach(logger::log);
        return Optional.ofNullable(result.tokens);
    }

    private Optional<Iterable<Token>> lexUncached(PPP4eLogger logger, CharSequence program) {
        TokenBuildingStream stream = new TokenBuildingStream();

        int line = 0;
        int column = 0;

        Character lastChar = null;

        boolean error = false;

        for (int i = 0; i < program.length(); i++) {
            char chr = program.charAt(i);

            if (stream.canAppend(chr)) {
                stream.append(chr);
            } else {
                logUnexpectedToken(logger, chr, line, column);
                stream.reset();
                error = true;
            }

            column++;
            if (shouldRecogniseNewline(chr, lastChar)) {
                line++;
                column = 0;
            }

            lastChar = chr;
        }

        if (error || !stream.isValidStream()) {
            return Optional.empty();
        } else {
            return Optional.of(stream.toTokens());
        }
    }

    private void logUnexpectedToken(PPP4eLogger logger, char chr, int line, int column) {
        Map<String, String> msgReplacements = Map.ofEntries(
                Map.entry("char", Character.toString(chr)),
                Map.entry("line", Integer.toString(line + 1)),
                Map.entry("column", Integer.toString(column + 1))
        );

        String msg = DefaultImplementationMessages.unexpectedToken.get(msgReplacements);

        logger.error(msg);
    }

    private boolean shouldRecogniseNewline(char chr, Character lastChar) {
        return chr == '\r' || (chr == '\n' && !(lastChar != null && lastChar == '\r'));
    }

    @Override
    public void applyCacheHint(CacheHint hint) {
        Objects.requireNonNull(hint);

        cache.applyCacheHint(hint);
    }
}
