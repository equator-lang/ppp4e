/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.logging;

import java.io.Serializable;
import java.util.Objects;

/**
 * A message to be sent to a {@link PPP4eLogger}.
 */
public class ParseMessage implements Serializable {

    private static final long serialVersionUID = 0L;

    protected final CharSequence message;
    protected final ParseMessageSeverity severity;

    /**
     * @param message the raw message
     * @param severity the severity of this message
     */
    public ParseMessage(CharSequence message, ParseMessageSeverity severity) {
        this.message = message;
        this.severity = severity;
    }

    /**
     * @return the raw message
     */
    public final CharSequence getMessage() {
        return message;
    }

    /**
     * @return the severity of this message
     */
    public final ParseMessageSeverity getSeverity() {
        return severity;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + getMessage() + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ParseMessage) {
            ParseMessage other = (ParseMessage) obj;

            return other.getMessage().equals(getMessage()) && other.getSeverity().equals(getSeverity());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMessage(), getSeverity());
    }
}
