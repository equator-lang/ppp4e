/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.logging;

/**
 * A system for logging parsing information to the user.
 */
public interface PPP4eLogger {

    /**
     * Log a message that was generated through parsing.
     *
     * @param message the message to be logged
     */
    void log(ParseMessage message);

    /**
     * Log a message with {@link ParseMessageSeverity#ERROR} severity.
     *
     * @param message the message to be logged
     */
    default void error(CharSequence message) {
        log(new ParseMessage(message, ParseMessageSeverity.ERROR));
    }

    /**
     * Log a message with {@link ParseMessageSeverity#WARN} severity.
     *
     * @param message the message to be logged
     */
    default void warn(CharSequence message) {
        log(new ParseMessage(message, ParseMessageSeverity.WARN));
    }

    /**
     * Log a message with {@link ParseMessageSeverity#INFO} severity.
     *
     * @param message the message to be logged
     */
    default void info(CharSequence message) {
        log(new ParseMessage(message, ParseMessageSeverity.INFO));
    }

    /**
     * Log a message with {@link ParseMessageSeverity#VERBOSE} severity.
     *
     * @param message the message to be logged
     */
    default void verbose(CharSequence message) {
        log(new ParseMessage(message, ParseMessageSeverity.VERBOSE));
    }
}
