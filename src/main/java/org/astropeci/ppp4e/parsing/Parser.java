/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.parsing;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.ast.ASTAssembler;
import org.astropeci.ppp4e.parsing.lexing.Lexer;

import java.util.Optional;

/**
 * Used to parse an Equator program into an {@link AST}. Can also be used to perform the role of a {@link Lexer}.
 */
public interface Parser extends Lexer, ASTAssembler, CacheHintReceiver {

    /**
     * Parse an Equator program into an {@link AST}. Returns an empty {@link Optional} if the source code could not be
     * parsed, otherwise it will return a full one.
     *
     * @param logger the logger used for reporting information
     * @param program the Equator program source code
     * @return the resulting {@link AST}, optionally
     */
    Optional<AST> parse(PPP4eLogger logger, CharSequence program);
}
