/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.parsing;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * A {@link ParsingContext} that can be reset from a {@link URL}.
 */
public interface URLParsingContext extends ParsingContext {

    /**
     * <p>
     *  Set the source code of this context to the source of the current {@link URL}. The {@link URL}s content is
     *  interpreted in the current {@link Charset}.
     * </p>
     *
     * <p>
     *  This method has the same effect as calling {@link #replaceSource(int, int, CharSequence)} with parameters
     *  <code>0</code>, <code>len</code> and <code>src</code>, where <code>len</code> is the length of the current
     *  source code and <code>src</code> is the source code obtained from the {@link URL}.
     * </p>
     *
     * @throws IOException if an underlying error occurs reading from the {@link URL}
     */
    void resetFromURL() throws IOException;

    /**
     * @return the current {@link URL}
     */
    URL getURL();

    /**
     * Sets the current {@link URL}.
     *
     * @param url the new {@link URL}
     */
    void setURL(URL url);

    /**
     * @return the current {@link Charset}
     */
    Charset getCharset();

    /**
     * Sets the current {@link Charset}
     *
     * @param charset the new {@link Charset}
     */
    void setCharset(Charset charset);
}
