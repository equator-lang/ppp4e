/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.parsing;

/**
 * A general purpose hint as to how frequently a caching system should store the result of the cached operation.
 */
public enum CacheHint {

    /**
     * Indicates that a very large amount of caching should occur. This indicates that, in general, everything that can
     * be cached should be cached unless memory is running low.
     */
    MAXIMUM_CACHING,

    /**
     * Indicates that a moderate amount of caching to apply; the implementation should choose an amount that it deems to
     * be an appropriate trade-off between performance and memory.
     */
    NORMAL_CACHING,

    /**
     * Indicates that caching should be enabled, but should be used sparingly. This prioritizes memory over caching.
     */
    SPARSE_CACHING,

    /**
     * Indicates that caching should never occur.
     */
    NEVER_CACHE
}
