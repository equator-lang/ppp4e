/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.parsing;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Optional;

/**
 * Represents the source code of an Equator program, a method of manipulating it and a method of parsing it.
 */
public interface ParsingContext extends CacheHintReceiver {

    /**
     * Returns the result of calling {@link CharSequence#length()} on {@link #getRaw()}.
     *
     * @return the length of {@link #getRaw()}
     */
    int getLength();

    /**
     * Returns the source code of this context. The returned char sequence is not immutable and will reflect any changes
     * that occur to the source code of this context.
     *
     * @return the source code of this context
     */
    CharSequence getRaw();

    /**
     * Edits the source of the Equator program that this parsing context refers to so that all the characters of the
     * source from the starting index to the starting index plus the length are replaced with the provided replacement.
     *
     * @param start the starting index
     * @param length the length of the replaced content
     * @param replacement the char sequence to replace with
     * @throws IndexOutOfBoundsException if the start or length are negative or their sum is greater than the length of
     *         the current source code
     */
    void replaceSource(int start, int length, CharSequence replacement);

    /**
     * Parse the source code into an {@link AST}. Returns an empty {@link Optional} if the source code could not be
     * parsed, otherwise it will return a full one. Even though the result of parsing is likely cached, any resulting
     * messages are applied to the given logger on every invocation.
     *
     * @param logger the logger used for reporting information
     * @return the resulting {@link AST}, optionally
     */
    Optional<AST> parse(PPP4eLogger logger);

    /**
     * Lex an Equator program into a sequence of {@link Token}s. If the program cannot be transformed into a sequence of
     * {@link Token}s, it will return an empty {@link Optional}, otherwise it will return a full one. The implementation
     * is permitted to apply messages to the logger only as the lexing occurs. This allows for lexing to occur lazily.
     * Even though the result of lexing is likely cached, any resulting messages are applied to the given logger on
     * every invocation.
     *
     * @param logger the logger used for reporting information
     * @return the resulting sequence of tokens, optionally
     */
    Optional<Iterable<Token>> lex(PPP4eLogger logger);
}
