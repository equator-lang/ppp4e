/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.parsing.lexing;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHintReceiver;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Optional;

/**
 * Used to transform an Equator program into a sequence of {@link Token}s.
 */
public interface Lexer extends CacheHintReceiver {

    /**
     * Lex an Equator program into a sequence of {@link Token}s. If the program cannot be transformed into a sequence of
     * {@link Token}s, it will return an empty {@link Optional}, otherwise it will return a full one. The implementation
     * is permitted to apply messages to the logger only as the lexing occurs. This allows for lexing to occur lazily.
     * Even though the result of lexing is likely cached, any resulting messages are applied to the given logger on
     * every invocation.
     *
     * @param logger the logger used for reporting information
     * @param program the program to be parsed
     * @return the resulting tokens, optionally
     */
    Optional<Iterable<Token>> lex(PPP4eLogger logger, CharSequence program);
}
