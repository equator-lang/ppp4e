/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.parsing.ast;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHintReceiver;
import org.astropeci.ppp4e.parsing.lexing.token.Token;

import java.util.Optional;

/**
 * Used to transform a sequence of {@link Token}s that represents an Equator program into an {@link AST}.
 */
public interface ASTAssembler extends CacheHintReceiver {

    /**
     * Assembles a sequence of {@link Token}s into an {@link AST}. This method logs any messages to the provided logger.
     * If the tokens could not be assembled into an {@link AST}, it will return an empty {@link Optional}, otherwise it
     * returns a full one.
     *
     * @param logger the {@link PPP4eLogger} to apply messages to
     * @param tokens the {@link Token}s to be assembled.
     * @return the resulting {@link AST}, optionally
     */
    Optional<AST> assemble(PPP4eLogger logger, Iterable<Token> tokens);
}
