/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.loading;

import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.ParsingContext;
import org.astropeci.ppp4e.parsing.URLParsingContext;

import java.net.URL;
import java.nio.charset.Charset;

/**
 * A factory for {@link ParsingContext}s and {@link URLParsingContext}s.
 */
public interface ParsingContextFactory {

    /**
     * Creates a {@link ParsingContext} with initial source code. Consider using
     * {@link #createParsingContext(URL, Charset, CacheHint)} if appropriate, because it may create a more efficient
     * {@link ParsingContext}.
     *
     * @param program the initial source code
     * @param cacheHint a hint as to how much caching should occur
     * @return a {@link ParsingContext} with the provided initial source code
     */
    ParsingContext createParsingContext(CharSequence program, CacheHint cacheHint);

    /**
     * Creates a {@link URLParsingContext} tied to the provided initial {@link URL} whose source code is interpreted
     * with the provided initial {@link Charset}.
     *
     * @param url the {@link URL} of the source code
     * @param charset the {@link Charset} to interpret the source code with
     * @param cacheHint a hint as to how much caching should occur
     * @return a {@link URLParsingContext} tied to the provided initial {@link URL}
     */
    URLParsingContext createParsingContext(URL url, Charset charset, CacheHint cacheHint);
}
