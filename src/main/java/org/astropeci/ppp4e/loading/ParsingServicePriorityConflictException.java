/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.loading;

/**
 * An {@link Exception} that signifies that a conflict of the value of {@link ParsingService#getPriority()} has occurred
 * and is preventing one from being chosen.
 */
public class ParsingServicePriorityConflictException extends Exception {

    private static final long serialVersionUID = 0L;

    /**
     * Defined by {@link Exception#Exception()}.
     */
    public ParsingServicePriorityConflictException() {
        super();
    }

    /**
     * Defined by {@link Exception#Exception(String)}.
     */
    public ParsingServicePriorityConflictException(String message) {
        super(message);
    }

    /**
     * Defined by {@link Exception#Exception(String, Throwable)}.
     */
    public ParsingServicePriorityConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Defined by {@link Exception#Exception(Throwable)}.
     */
    public ParsingServicePriorityConflictException(Throwable cause) {
        super(cause);
    }

    /**
     * Defined by {@link Exception#Exception(String, Throwable, boolean, boolean)}.
     */
    public ParsingServicePriorityConflictException(String message, Throwable cause, boolean enableSuppression,
                                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
