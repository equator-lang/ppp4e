/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.loading;

import org.astropeci.ppp4e.defaultimpl.DefaultParsingService;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.Parser;
import org.astropeci.ppp4e.parsing.ParsingContext;
import org.astropeci.ppp4e.parsing.URLParsingContext;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Finds and loads instances of {@link ParsingService} at runtime. The easiest way to scan for {@link ParsingService}s
 * and load an appropriate one is by using the {@link #getPreferredImplementation()} method.
 */
public class DynamicLoader implements ParserFactory, ParsingContextFactory {

    /**
     * Returns the preferred implementation that is in the given sequence of implementations. An implementation is
     * preferred if its {@link ParsingService#getPriority()} returns a value larger than any other. If two
     * implementations return the same value for {@link ParsingService#getPriority()}, an instance of
     * {@link ParsingServicePriorityConflictException} is thrown. If the sequence of implementations is empty, an
     * instance of {@link DefaultParsingService} is returned.
     *
     * @param implementations the sequence of implementations from which to choose a preferred implementation
     * @return the implementation with the highest priority from the given implementations, or a default implementation
     *         if it is empty
     * @throws ParsingServicePriorityConflictException if two implementations have the same priority
     */
    public ParsingService getPreferredImplementation(Iterable<ParsingService> implementations)
            throws ParsingServicePriorityConflictException {

        ParsingService winner = null;
        Set<Integer> usedPriorities = new HashSet<>();
        for (ParsingService implementation : implementations) {
            // Disallow implementations with the same priority.
            if (!usedPriorities.add(implementation.getPriority())) {
                throw new ParsingServicePriorityConflictException("conflicting priorities");
            }

            if (winner == null || implementation.getPriority() > winner.getPriority()) {
                winner = implementation;
            }
        }

        // Fallback implementation.
        if (winner == null) {
            winner = new DefaultParsingService();
        }

        return winner;
    }

    /**
     * Returns the result of applying the result of {@link #getImplementations()} to
     * {@link #getPreferredImplementation(Iterable)}. In other words, it finds the most prioritized implementations of
     * all those that are registered.
     *
     * @return the most prioritized implementations of all those that are registered
     * @throws ParsingServicePriorityConflictException if two implementations have the same priority
     * @throws java.util.ServiceConfigurationError if loading a sequence of implementations fails
     */
    public ParsingService getPreferredImplementation() throws ParsingServicePriorityConflictException {
        return getPreferredImplementation(getImplementations());
    }

    /**
     * Returns a sequence of all the registered {@link ParsingService} implementations. This method behaves as if by the
     * implementation:
     * <code>{@link ServiceLoader}.load({@link ParsingService}.class)</code>
     *
     * @return a sequence of all the registered {@link ParsingService} implementations
     * @throws java.util.ServiceConfigurationError if loading a sequence of implementations fails
     */
    public Iterable<ParsingService> getImplementations() {
        // Attempts to cache will result in unresponsiveness to classpath changes.
        return ServiceLoader.load(ParsingService.class);
    }

    /**
     * Delegates to the {@link ParsingService#createParser(CacheHint)} method of {@link #getPreferredImplementation()}.
     * Any resulting {@link ParsingServicePriorityConflictException}s are wrapped in a {@link RuntimeException}.
     *
     * @inheritDoc
     *
     * @throws RuntimeException if {@link #getPreferredImplementation()} throws a
     *         {@link ParsingServicePriorityConflictException}
     */
    @Override
    public Parser createParser(CacheHint cacheHint) {
        try {
            return getPreferredImplementation().createParser(cacheHint);
        } catch (ParsingServicePriorityConflictException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Delegates to the {@link ParsingService#createParsingContext(CharSequence, CacheHint)} method of
     * {@link #getPreferredImplementation()}. Any resulting {@link ParsingServicePriorityConflictException}s are wrapped
     * in a {@link RuntimeException}.
     *
     * @inheritDoc
     *
     * @throws RuntimeException if {@link #getPreferredImplementation()} throws a
     *         {@link ParsingServicePriorityConflictException}
     */
    @Override
    public ParsingContext createParsingContext(CharSequence program, CacheHint cacheHint) {
        try {
            return getPreferredImplementation().createParsingContext(program, cacheHint);
        } catch (ParsingServicePriorityConflictException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Delegates to the {@link ParsingService#createParsingContext(URL, Charset, CacheHint)} method of
     * {@link #getPreferredImplementation()}. Any resulting {@link ParsingServicePriorityConflictException}s are wrapped
     * in a {@link RuntimeException}.
     *
     * @inheritDoc
     *
     * @throws RuntimeException if {@link #getPreferredImplementation()} throws a
     *         {@link ParsingServicePriorityConflictException}
     */
    @Override
    public URLParsingContext createParsingContext(URL url, Charset charset, CacheHint cacheHint) {
        try {
            return getPreferredImplementation().createParsingContext(url, charset, cacheHint);
        } catch (ParsingServicePriorityConflictException e) {
            throw new RuntimeException(e);
        }
    }
}
