module org.astropeci.ppp4e {
    requires commons.lang3;

    uses org.astropeci.ppp4e.loading.ParsingService;

    exports org.astropeci.ppp4e.loading;

    exports org.astropeci.ppp4e.logging;

    exports org.astropeci.ppp4e.parsing;
    exports org.astropeci.ppp4e.parsing.ast;
    exports org.astropeci.ppp4e.parsing.classification;
    exports org.astropeci.ppp4e.parsing.lexing;
    exports org.astropeci.ppp4e.parsing.lexing.token;
}