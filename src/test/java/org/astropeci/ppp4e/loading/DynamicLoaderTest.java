/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.loading;

import org.astropeci.ppp4e.defaultimpl.DefaultParsingService;
import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.Parser;
import org.astropeci.ppp4e.parsing.ParsingContext;
import org.astropeci.ppp4e.parsing.URLParsingContext;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.lexing.token.Token;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

class DynamicLoaderTest {

    /**
     * A {@link ParsingService} that defaults its implementations to throwing an {@link UnsupportedOperationException}.
     */
    class DummyParsingService implements ParsingService {
        @Override
        public int getPriority() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Parser createParser(CacheHint cacheHint) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ParsingContext createParsingContext(CharSequence program, CacheHint cacheHint) {
            throw new UnsupportedOperationException();
        }

        @Override
        public URLParsingContext createParsingContext(URL url, Charset charset, CacheHint cacheHint) {
            throw new UnsupportedOperationException();
        }
    }

    ParsingService priorityOnlyParsingService(int priority) {
        return new DummyParsingService() {
            @Override
            public int getPriority() {
                return priority;
            }
        };
    }

    @Test
    void getPreferredImplementationReturnsHighestPriorityImplementation()
            throws ParsingServicePriorityConflictException {

        DynamicLoader loader = new DynamicLoader();
        Iterable<ParsingService> implementations = List.of(
                priorityOnlyParsingService(Integer.MIN_VALUE),
                priorityOnlyParsingService(-64),
                priorityOnlyParsingService(-1),
                priorityOnlyParsingService(0),
                priorityOnlyParsingService(2),
                priorityOnlyParsingService(5)

        );
        ParsingService chosen = loader.getPreferredImplementation(implementations);

        assertEquals(chosen.getPriority(), 5);
    }

    @Test
    void getPreferredImplementationReturnsCorrectDefault() throws ParsingServicePriorityConflictException {
        DynamicLoader loader = new DynamicLoader();
        ParsingService chosen = loader.getPreferredImplementation(List.of());

        assertTrue(chosen instanceof DefaultParsingService);
    }

    @Test
    void getPreferredImplementationFailsWithDuplicatePriorities() {
        DynamicLoader loader = new DynamicLoader();
        Iterable<ParsingService> implementations = List.of(
                priorityOnlyParsingService(-3),
                priorityOnlyParsingService(-2),
                priorityOnlyParsingService(-2),
                priorityOnlyParsingService(7)
        );

        assertThrows(ParsingServicePriorityConflictException.class,
                () -> loader.getPreferredImplementation(implementations));
    }

    @Test
    void createParserDelegatesToPreferredImplementation() {
        Parser returnedParser = new Parser() {
            @Override
            public Optional<AST> parse(PPP4eLogger logger, CharSequence program) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Optional<AST> assemble(PPP4eLogger logger, Iterable<Token> tokens) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Optional<Iterable<Token>> lex(PPP4eLogger logger, CharSequence program) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void applyCacheHint(CacheHint hint) {
                throw new UnsupportedOperationException();
            }
        };

        DynamicLoader loader = new DynamicLoader() {
            @Override
            public ParsingService getPreferredImplementation() {
                return new DummyParsingService() {
                    @Override
                    public Parser createParser(CacheHint cacheHint) {
                        return returnedParser;
                    }
                };
            }
        };

        assertEquals(loader.createParser(CacheHint.NORMAL_CACHING), returnedParser);
    }

    @Test
    void createParserWrapsParsingServicePriorityConflictExceptions() {
        DynamicLoader loader = new DynamicLoader() {
            @Override
            public ParsingService getPreferredImplementation() throws ParsingServicePriorityConflictException {
                throw new ParsingServicePriorityConflictException();
            }
        };

        assertThrows(RuntimeException.class, () -> loader.createParser(CacheHint.NORMAL_CACHING));
    }

    @Test
    void createParsingContextWithSourceDelegatesToPreferredImplementation() {
        AtomicReference<CharSequence> receivedProgramSource = new AtomicReference<>();
        AtomicReference<CacheHint> receivedCacheHint = new AtomicReference<>();

        ParsingContext returnedParsingContext = new ParsingContext() {
            @Override
            public int getLength() {
                throw new UnsupportedOperationException();
            }

            @Override
            public CharSequence getRaw() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void replaceSource(int start, int length, CharSequence replacement) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Optional<AST> parse(PPP4eLogger logger) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Optional<Iterable<Token>> lex(PPP4eLogger logger) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void applyCacheHint(CacheHint hint) {
                throw new UnsupportedOperationException();
            }
        };

        DynamicLoader loader = new DynamicLoader() {
            @Override
            public ParsingService getPreferredImplementation() {
                return new DummyParsingService() {
                    @Override
                    public ParsingContext createParsingContext(CharSequence program, CacheHint cacheHint) {
                        receivedProgramSource.set(program);
                        receivedCacheHint.set(cacheHint);
                        return returnedParsingContext;
                    }
                };
            }
        };

        String passedProgramSource = "example text";
        CacheHint passedCacheHint = CacheHint.MAXIMUM_CACHING;

        assertEquals(loader.createParsingContext(passedProgramSource, passedCacheHint), returnedParsingContext);
        assertEquals(receivedProgramSource.get(), passedProgramSource);
        assertEquals(receivedCacheHint.get(), passedCacheHint);
    }

    @Test
    void createParsingContextWithSourceWrapsParsingServicePriorityConflictExceptions() {
        DynamicLoader loader = new DynamicLoader() {
            @Override
            public ParsingService getPreferredImplementation() throws ParsingServicePriorityConflictException {
                throw new ParsingServicePriorityConflictException();
            }
        };

        assertThrows(RuntimeException.class, () -> loader.createParsingContext("", CacheHint.NORMAL_CACHING));
    }

    @Test
    void createParsingContextWithURLDelegatesToPreferredImplementation() throws MalformedURLException {
        AtomicReference<URL> receivedURL = new AtomicReference<>();
        AtomicReference<Charset> receivedCharset = new AtomicReference<>();
        AtomicReference<CacheHint> receivedCacheHint = new AtomicReference<>();

        URLParsingContext returnedParsingContext = new URLParsingContext() {
            @Override
            public void resetFromURL() {
                throw new UnsupportedOperationException();
            }

            @Override
            public URL getURL() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void setURL(URL url) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Charset getCharset() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void setCharset(Charset charset) {
                throw new UnsupportedOperationException();
            }

            @Override
            public int getLength() {
                throw new UnsupportedOperationException();
            }

            @Override
            public CharSequence getRaw() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void replaceSource(int start, int length, CharSequence replacement) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Optional<AST> parse(PPP4eLogger logger) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Optional<Iterable<Token>> lex(PPP4eLogger logger) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void applyCacheHint(CacheHint hint) {
                throw new UnsupportedOperationException();
            }
        };

        DynamicLoader loader = new DynamicLoader() {
            @Override
            public ParsingService getPreferredImplementation() {
                return new DummyParsingService() {
                    @Override
                    public URLParsingContext createParsingContext(URL url, Charset charset, CacheHint cacheHint) {
                        receivedURL.set(url);
                        receivedCharset.set(charset);
                        receivedCacheHint.set(cacheHint);
                        return returnedParsingContext;
                    }
                };
            }
        };

        URL passedURL = new URL("file://the/content/of/this/url/does/not/matter");
        Charset passedCharset = StandardCharsets.US_ASCII;
        CacheHint passedCacheHint = CacheHint.MAXIMUM_CACHING;

        assertEquals(loader.createParsingContext(passedURL, passedCharset, passedCacheHint), returnedParsingContext);
        // Test for reference equality, calling the equals method of URL is a bad idea.
        assertSame(receivedURL.get(), passedURL);
        assertEquals(receivedCharset.get(), passedCharset);
        assertEquals(receivedCacheHint.get(), passedCacheHint);
    }

    @Test
    void createParsingContextWithURLWrapsParsingServicePriorityConflictExceptions() throws MalformedURLException {
        DynamicLoader loader = new DynamicLoader() {
            @Override
            public ParsingService getPreferredImplementation() throws ParsingServicePriorityConflictException {
                throw new ParsingServicePriorityConflictException();
            }
        };

        URL passedURL = new URL("file://the/content/of/this/url/does/not/matter");

        assertThrows(RuntimeException.class, () -> loader.createParsingContext(passedURL, StandardCharsets.US_ASCII,
                CacheHint.NORMAL_CACHING));
    }
}