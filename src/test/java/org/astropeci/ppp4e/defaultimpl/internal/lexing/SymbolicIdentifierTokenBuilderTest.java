/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SymbolicIdentifierTokenBuilderTest {

    static Stream<Arguments> validIdentifiers() {
        return Stream.of(
                "+",
                "~!$%^&*-=+\\|:<>/?",
                "+_+-_-==",
                ".-~<>~-",
                ".+:_:-="
        ).map(Arguments::of);
    }

    static Stream<Arguments> invalidIdentifiers() {
        return Stream.of(
                "",
                " ",
                "\t",
                "\n",
                "a",
                ".+.",
                "😺",
                "\u0000", // ASCII null character.
                "\uD835" // An unfinished surrogate pair.
        ).map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("validIdentifiers")
    void isValidTokenReturnsTrueWhenFilled(String content) {
        TokenBuilder builder = new SymbolicIdentifierTokenBuilder();
        for (char c : content.toCharArray()) {
            builder = builder.append(c);
        }

        assertTrue(builder.isValidToken());
    }

    @Test
    void isValidTokenReturnsFalseWhenEmpty() {
        TokenBuilder builder = new SymbolicIdentifierTokenBuilder();

        assertFalse(builder.isValidToken());
    }

    @Test
    void toTokenReflectsContent() {
        TokenBuilder builder1 = new SymbolicIdentifierTokenBuilder().append('+');
        TokenBuilder builder2 = new SymbolicIdentifierTokenBuilder().append('.').append('_');

        assertEquals(builder1.toToken().getRaw(), "+");
        assertEquals(builder2.toToken().getRaw(), "._");
    }

    @Test
    void toTokenIsSymbolic() {
        SymbolicIdentifierTokenBuilder builder = new SymbolicIdentifierTokenBuilder().append('+');

        assertTrue(builder.toToken().isSymbolic());
    }

    @Test
    void toTokenFailsForInvalidToken() {
        TokenBuilder builder = new SymbolicIdentifierTokenBuilder();

        assertThrows(IllegalStateException.class, builder::toToken);
    }

    @ParameterizedTest
    @ValueSource(chars = {
            '+',
            '/',
            '&',
            '\\',
            ':',
            '_'
    })
    void canAppendAcceptsValidCharacters(char validChar) {
        TokenBuilder builder1 = new SymbolicIdentifierTokenBuilder();
        TokenBuilder builder2 = new SymbolicIdentifierTokenBuilder().append('+');

        assertTrue(builder1.canAppend(validChar));
        assertTrue(builder2.canAppend(validChar));
    }

    @ParameterizedTest
    @ValueSource(chars = {
            'a',
            'A',
            '0',
            '9',
            ' ',
            '\t',
            '\n'
    })
    void canAppendRejectsInvalidCharacters(char invalidChar) {
        TokenBuilder builder1 = new SymbolicIdentifierTokenBuilder();
        TokenBuilder builder2 = new SymbolicIdentifierTokenBuilder().append('+');

        assertFalse(builder1.canAppend(invalidChar));
        assertFalse(builder2.canAppend(invalidChar));
    }

    @Test
    void canAppendAcceptsPeriodAtStart() {
        TokenBuilder builder = new SymbolicIdentifierTokenBuilder();

        assertTrue(builder.canAppend('.'));
    }

    @Test
    void canAppendRejectsPeriodsNotAtStart() {
        TokenBuilder builder = new SymbolicIdentifierTokenBuilder().append('+');

        assertFalse(builder.canAppend('.'));
    }

    @ParameterizedTest
    @MethodSource("validIdentifiers")
    void isValidSymbolicIdentifierReturnsTrueForValidIdentifier(String identifier) {
        assertTrue(SymbolicIdentifierTokenBuilder.isValidSymbolicIdentifier(identifier));
    }

    @ParameterizedTest
    @MethodSource("invalidIdentifiers")
    void isValidSymbolicIdentifierReturnsFalseForInvalidIdentifier(String identifier) {
        assertFalse(SymbolicIdentifierTokenBuilder.isValidSymbolicIdentifier(identifier));
    }
}