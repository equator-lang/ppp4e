/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.caching;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;

class DefaultMaximumCachingSubSystemTest {

    @Test
    void isCachedReturnsFalseWhenNotCached() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();

        assertFalse(subSystem.isCached("example"));
    }

    @Test
    void isCachedReturnsTrueWhenCached() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();
        subSystem.put("number", 42);

        assertTrue(subSystem.isCached("number"));
    }

    @Test
    void getOrThrowFailsWhenNotCached() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();

        assertThrows(NoSuchElementException.class, () -> subSystem.getOrThrow("example"));
    }

    @Test
    void getOrThrowReturnsValueWhenCached() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();
        subSystem.put("number", 42);

        assertEquals(subSystem.getOrThrow("number"), Integer.valueOf(42));
    }

    @Test
    void invalidateUncachesKey() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();
        subSystem.put("number", 42);
        subSystem.invalidate("number");

        assertFalse(subSystem.isCached("number"));
    }

    @Test
    void accessDirectlyReturnsWhenCached() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();
        subSystem.put("number", 42);

        assertEquals(subSystem.access("number", () -> fail("")), Integer.valueOf(42));
    }

    @Test
    void accessEvaluatesSupplierWhenNotCached() {
        CachingSubSystem<String, Integer> subSystem = new DefaultMaximumCachingSubSystem<>();

        AtomicBoolean supplierCalled = new AtomicBoolean();

        assertEquals(subSystem.access("number", () -> {
            supplierCalled.set(true);
            return 42;
        }), Integer.valueOf(42));

        assertTrue(supplierCalled.get());
    }
}