/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.AtSignToken;
import org.astropeci.ppp4e.parsing.lexing.token.Token;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MultiplexedTokenBuilderTest {

    class DummyTokenBuilder implements TokenBuilder {

        DummyTokenBuilder() { }

        @Override
        public boolean isEmpty() {
            throw new UnsupportedOperationException();
        }

        @Override
        public CharSequence getRaw() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isValidToken() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Token toToken() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean canAppend(char c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public TokenBuilder append(char c) {
            throw new UnsupportedOperationException();
        }
    }

    class ValidDummyTokenBuilder extends DummyTokenBuilder {

        @Override
        public boolean isValidToken() {
            return true;
        }
    }

    class InvalidDummyTokenBuilder extends DummyTokenBuilder {

        @Override
        public boolean isValidToken() {
            return false;
        }
    }

    class AppendableDummyTokenBuilder extends DummyTokenBuilder {

        @Override
        public boolean canAppend(char c) {
            return true;
        }
    }

    class NotAppendableDummyTokenBuilder extends DummyTokenBuilder {

        @Override
        public boolean canAppend(char c) {
            return false;
        }
    }

    @Test
    void chainedFailsForNoBuilders() {
        assertThrows(IllegalArgumentException.class, () -> MultiplexedTokenBuilder.chained(List.of()));
    }

    @Test
    void chainedReturnsBuilderWhenOneBuilderIsGiven() {
        TokenBuilder builder = new DummyTokenBuilder();

        assertEquals(builder, MultiplexedTokenBuilder.chained(List.of(builder)));
    }

    @Test
    void chainedReturnsCorrectMultiplexedWhenTwoBuildersAreGiven() {
        TokenBuilder builder1 = new DummyTokenBuilder();
        TokenBuilder builder2 = new DummyTokenBuilder();

        MultiplexedTokenBuilder multiplexed = (MultiplexedTokenBuilder) MultiplexedTokenBuilder.chained(
                List.of(builder1, builder2));

        assertEquals(Set.of(builder1, builder2), multiplexed.getUnderlying());
    }

    @Test
    void chainedNestsWhenManyBuildersAreGiven() {
        TokenBuilder builder1 = new DummyTokenBuilder();
        TokenBuilder builder2 = new DummyTokenBuilder();
        TokenBuilder builder3 = new DummyTokenBuilder();
        TokenBuilder builder4 = new DummyTokenBuilder();

        MultiplexedTokenBuilder multiplexed = (MultiplexedTokenBuilder) MultiplexedTokenBuilder.chained(
                List.of(builder1, builder2, builder3, builder4));

        Set<TokenBuilder> expected = Set.of(builder1, builder2, builder3, builder4);

        Set<TokenBuilder> result = new HashSet<>();
        for (TokenBuilder b1 : multiplexed.getUnderlying()) {
            if (b1 instanceof MultiplexedTokenBuilder) {
                for (TokenBuilder b2 : ((MultiplexedTokenBuilder) b1).getUnderlying()) {
                    if (b2 instanceof MultiplexedTokenBuilder) {
                        result.addAll(((MultiplexedTokenBuilder) b2).getUnderlying());
                    } else {
                        result.add(b2);
                    }
                }
            } else {
                result.add(b1);
            }
        }

        assertEquals(expected, result);
    }

    @Test
    void getUnderlyingReturnsCorrectElements() {
        TokenBuilder builder1 = new DummyTokenBuilder();
        TokenBuilder builder2 = new DummyTokenBuilder();
        MultiplexedTokenBuilder multiplexed = new MultiplexedTokenBuilder(builder1, builder2);

        Set<TokenBuilder> result = multiplexed.getUnderlying();
        Set<TokenBuilder> expected = Set.of(builder1, builder2);

        assertEquals(result, expected);
    }

    @Test
    void isEmptyReturnsTrueWhenBothBuildersAreEmpty() {
        TokenBuilder builder = new DummyTokenBuilder() {
            @Override
            public boolean isEmpty() {
                return true;
            }
        };
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(builder, builder);

        assertTrue(multiplexed.isEmpty());
    }

    @Test
    void isEmptyReturnsFalseWhenBothBuildersAreFull() {
        TokenBuilder builder = new DummyTokenBuilder() {
            @Override
            public boolean isEmpty() {
                return false;
            }
        };
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(builder, builder);

        assertFalse(multiplexed.isEmpty());
    }

    @Test
    void isEmptyFailsWhenBuildersDisagree() {
        TokenBuilder builder1 = new DummyTokenBuilder() {
            @Override
            public boolean isEmpty() {
                return false;
            }
        };
        TokenBuilder builder2 = new DummyTokenBuilder() {
            @Override
            public boolean isEmpty() {
                return true;
            }
        };
        TokenBuilder multiplexed1 = new MultiplexedTokenBuilder(builder1, builder2);
        TokenBuilder multiplexed2 = new MultiplexedTokenBuilder(builder2, builder1);


        assertThrows(IllegalStateException.class, multiplexed1::isEmpty);
        assertThrows(IllegalStateException.class, multiplexed2::isEmpty);
    }

    @Test
    void getRawReturnsContent() {
        TokenBuilder builder = new DummyTokenBuilder() {
            @Override
            public CharSequence getRaw() {
                return "42";
            }
        };
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(builder, builder);

        assertEquals(multiplexed.getRaw(), "42");
    }

    @Test
    void getRawDoesNotThrowIllegalStateExceptionWhenBuildersReturnDifferentCharSequenceTypes() {
        TokenBuilder builder1 = new DummyTokenBuilder() {
            @Override
            public CharSequence getRaw() {
                return new StringBuilder("42");
            }
        };
        TokenBuilder builder2 = new DummyTokenBuilder() {
            @Override
            public CharSequence getRaw() {
                return "42";
            }
        };
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(builder1, builder2);

        multiplexed.getRaw();
    }

    @Test
    void getRawFailsWhenBuildersDisagree() {
        TokenBuilder builder1 = new DummyTokenBuilder() {
            @Override
            public CharSequence getRaw() {
                return "number";
            }
        };
        TokenBuilder builder2 = new DummyTokenBuilder() {
            @Override
            public CharSequence getRaw() {
                return "42";
            }
        };
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(builder1, builder2);

        assertThrows(IllegalStateException.class, multiplexed::getRaw);
    }

    @Test
    void isValidTokenReturnsTrueWhenOneBuilderIsValid() {
        TokenBuilder multiplexed1 = new MultiplexedTokenBuilder(
                new ValidDummyTokenBuilder(), new InvalidDummyTokenBuilder());
        TokenBuilder multiplexed2 = new MultiplexedTokenBuilder(
                new InvalidDummyTokenBuilder(), new ValidDummyTokenBuilder());

        assertTrue(multiplexed1.isValidToken());
        assertTrue(multiplexed2.isValidToken());
    }

    @Test
    void isValidTokenReturnsFalseWhenNoBuildersAreValid() {
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(
                new InvalidDummyTokenBuilder(), new InvalidDummyTokenBuilder());

        assertFalse(multiplexed.isValidToken());
    }

    @Test
    void isValidTokenReturnsFalseWhenBothBuildersAreValid() {
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(
                new ValidDummyTokenBuilder(), new ValidDummyTokenBuilder());

        assertFalse(multiplexed.isValidToken());
    }

    @Test
    void toTokenReturnsCorrectToken() {
        Token token = new AtSignToken();
        TokenBuilder builder = new ValidDummyTokenBuilder() {
            @Override
            public Token toToken() {
                return token;
            }
        };
        TokenBuilder multiplexed1 = new MultiplexedTokenBuilder(builder, new InvalidDummyTokenBuilder());
        TokenBuilder multiplexed2 = new MultiplexedTokenBuilder(new InvalidDummyTokenBuilder(), builder);

        assertEquals(multiplexed1.toToken(), token);
        assertEquals(multiplexed2.toToken(), token);
    }

    @Test
    void toTokenFailsWhenNotValid() {
        TokenBuilder multiplexed1 = new MultiplexedTokenBuilder(
                new ValidDummyTokenBuilder(), new ValidDummyTokenBuilder());
        TokenBuilder multiplexed2 = new MultiplexedTokenBuilder(
                new InvalidDummyTokenBuilder(), new InvalidDummyTokenBuilder());

        assertThrows(IllegalStateException.class, multiplexed1::toToken);
        assertThrows(IllegalStateException.class, multiplexed2::toToken);
    }

    @Test
    void canAppendReturnsTrueWhenAnyBuilderCanBeAppendedTo() {
        TokenBuilder multiplexed1 = new MultiplexedTokenBuilder(
                new AppendableDummyTokenBuilder(), new AppendableDummyTokenBuilder());
        TokenBuilder multiplexed2 = new MultiplexedTokenBuilder(
                new AppendableDummyTokenBuilder(), new NotAppendableDummyTokenBuilder());
        TokenBuilder multiplexed3 = new MultiplexedTokenBuilder(
                new NotAppendableDummyTokenBuilder(), new AppendableDummyTokenBuilder());

        for (char c = ' '; c <= '~'; c++) {
            assertTrue(multiplexed1.canAppend(c));
            assertTrue(multiplexed2.canAppend(c));
            assertTrue(multiplexed3.canAppend(c));
        }
    }

    @Test
    void canAppendReturnsFalseWhenNoBuildersCanBeAppendedTo() {
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(
                new NotAppendableDummyTokenBuilder(), new NotAppendableDummyTokenBuilder());

        for (char c = ' '; c <= '~'; c++) {
            assertFalse(multiplexed.canAppend(c));
        }
    }

    @Test
    void appendReturnsCorrectMultiplexedBuilderWhenBothBuildersCanBeAppendedTo() {
        TokenBuilder expected1 = new DummyTokenBuilder();
        TokenBuilder expected2 = new DummyTokenBuilder();

        TokenBuilder builder1 = new AppendableDummyTokenBuilder() {
            @Override
            public TokenBuilder append(char c) {
                return expected1;
            }
        };
        TokenBuilder builder2 = new AppendableDummyTokenBuilder() {
            @Override
            public TokenBuilder append(char c) {
                return expected2;
            }
        };

        TokenBuilder multiplexed = new MultiplexedTokenBuilder(builder1, builder2);

        assertEquals(((MultiplexedTokenBuilder) multiplexed.append('a')).getUnderlying(), Set.of(expected1, expected2));
    }

    @Test
    void appendReturnsBuilderAppendResultWhenOneBuilderCanBeAppendedTo() {
        TokenBuilder expected = new DummyTokenBuilder();
        TokenBuilder builder = new AppendableDummyTokenBuilder() {
            @Override
            public TokenBuilder append(char c) {
                return expected;
            }
        };
        TokenBuilder multiplexed1 = new MultiplexedTokenBuilder(builder, new NotAppendableDummyTokenBuilder());
        TokenBuilder multiplexed2 = new MultiplexedTokenBuilder(new NotAppendableDummyTokenBuilder(), builder);

        assertEquals(multiplexed1.append('a'), expected);
        assertEquals(multiplexed2.append('a'), expected);
    }

    @Test
    void appendFailsWhenNoBuildersCanBeAppendedTo() {
        TokenBuilder multiplexed = new MultiplexedTokenBuilder(
                new NotAppendableDummyTokenBuilder(), new NotAppendableDummyTokenBuilder());

        assertThrows(IllegalStateException.class, () -> multiplexed.append('a'));
    }
}