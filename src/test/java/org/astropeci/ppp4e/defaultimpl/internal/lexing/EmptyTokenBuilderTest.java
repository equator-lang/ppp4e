/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class EmptyTokenBuilderTest {

    @Test
    void isEmptyReturnsTrue() {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertTrue(builder.isEmpty());
    }

    @Test
    void getRawReturnsEmptyString() {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertEquals(builder.getRaw(), "");
    }

    @Test
    void isValidTokenReturnsFalse() {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertFalse(builder.isValidToken());
    }

    @Test
    void toTokenFails() {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertThrows(IllegalStateException.class, builder::toToken);
    }

    static Stream<Arguments> validTokenStarts() {
        return Stream.of(
                '`',
                '~',
                '1',
                '!',
                '2',
                '@',
                '3',
                '#',
                '$',
                '%',
                '^',
                '&',
                '*',
                '(',
                ')',
                '-',
                '_',
                '=',
                '+',
                'a',
                'b',
                'c',
                'π',
                'ﬖ',
                '[',
                '{',
                ']',
                '}',
                '\\',
                '|',
                ';',
                '\'',
                '"',
                ',',
                '<',
                '.',
                '>',
                '/',
                '?',
                ' ',
                '\t',
                '\n',
                '\uD835' // Unfinished surrogate pair.
        ).map(Arguments::of);
    }

    static Stream<Arguments> invalidTokenStarts() {
        return Stream.of(
                '\u0000', // ASCII null character.
                '\u0007' // ASCII bell character.
        ).map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("validTokenStarts")
    void canAppendReturnsTrueForValidProgramStarts(char c) {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertTrue(builder.canAppend(c));
    }

    @ParameterizedTest
    @MethodSource("invalidTokenStarts")
    void canAppendReturnsFalseForInvalidProgramStarts(char c) {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertFalse(builder.canAppend(c));
    }

    @ParameterizedTest
    @MethodSource("validTokenStarts")
    void appendReturnsSuccessfullyForValidProgramStarts(char c) {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertNotNull(builder.append(c));
    }

    @ParameterizedTest
    @MethodSource("invalidTokenStarts")
    void appendFailsForInvalidProgramStarts(char c) {
        EmptyTokenBuilder builder = new EmptyTokenBuilder();

        assertThrows(IllegalStateException.class, () -> builder.append(c));
    }
}