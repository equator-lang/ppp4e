/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class StandardIdentifierTokenBuilderTest {

    static Stream<Arguments> validIdentifiers() {
        return Stream.of(
                "abc",
                "42",
                "42a42b42c",
                ".abc",
                ".42",
                ".42a42b42c",
                "_a_b_c_",
                "_4_2_",
                ".__abc__42",
                "._42a_42b_42c_",
                ".π_ﬖ_",
                "𝟉𝟇x𝟁" // Contains surrogate pairs.
        ).map(Arguments::of);
    }

    static Stream<Arguments> invalidIdentifiers() {
        return Stream.of(
                "",
                " ",
                " abc",
                "abc ",
                "\t",
                "\n",
                ".",
                "._",
                "_",
                "..",
                "...",
                ".a.b.c.",
                ".4.2.",
                "=",
                "+",
                "}",
                "{_}",
                "😺",
                "\u0000", // ASCII null character.
                "\uD835", // An unfinished surrogate pair.
                "\uD835\u0041", // An invalid surrogate pair.
                "𝟉𝟇x𝟁\uD835" // Contains surrogate pairs, including an unfinished one.
        ).map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("validIdentifiers")
    void isValidTokenReturnsTrueWhenNotEmpty(String identifier) {
        TokenBuilder builder = new StandardIdentifierTokenBuilder();
        for (char c : identifier.toCharArray()) {
            builder = builder.append(c);
        }

        assertTrue(builder.isValidToken());
    }

    @Test
    void isValidTokenReturnsFalseWhenSurrogatePairIsUnfinished() {
        TokenBuilder builder = new StandardIdentifierTokenBuilder().append('\uD835');

        assertFalse(builder.isValidToken());
    }

    @Test
    void isValidTokenReturnsFalseWhenEmpty() {
        TokenBuilder builder = new StandardIdentifierTokenBuilder();

        assertFalse(builder.isValidToken());
    }

    @Test
    void isValidTokenReturnsFalseWhenAmbiguousWithSymbolicIdentifier() {
        TokenBuilder builder1 = new StandardIdentifierTokenBuilder().append('.');
        TokenBuilder builder2 = new StandardIdentifierTokenBuilder().append('.').append('_');
        TokenBuilder builder3 = new StandardIdentifierTokenBuilder().append('_');

        assertFalse(builder1.isValidToken());
        assertFalse(builder2.isValidToken());
        assertFalse(builder3.isValidToken());
    }

    @Test
    void toTokenReflectsContent() {
        TokenBuilder builder1 = new StandardIdentifierTokenBuilder().append('a');
        TokenBuilder builder2 = new StandardIdentifierTokenBuilder().append('4').append('2');

        assertEquals(builder1.toToken().getRaw(), "a");
        assertEquals(builder2.toToken().getRaw(), "42");
    }

    @Test
    void toTokenIsNotSymbolic() {
        StandardIdentifierTokenBuilder builder1 = new StandardIdentifierTokenBuilder().append('a');
        StandardIdentifierTokenBuilder builder2 = new StandardIdentifierTokenBuilder().append('4').append('2');

        assertFalse(builder1.toToken().isSymbolic());
        assertFalse(builder2.toToken().isSymbolic());
    }

    @Test
    void toTokenFailsForInvalidToken() {
        StandardIdentifierTokenBuilder builder1 = new StandardIdentifierTokenBuilder();
        StandardIdentifierTokenBuilder builder2 = new StandardIdentifierTokenBuilder().append('.').append('_');
        // Unfinished surrogate pair.
        StandardIdentifierTokenBuilder builder3 = new StandardIdentifierTokenBuilder().append('\uD835');

        assertThrows(IllegalStateException.class, builder1::toToken);
        assertThrows(IllegalStateException.class, builder2::toToken);
        assertThrows(IllegalStateException.class, builder3::toToken);
    }

    @ParameterizedTest
    @ValueSource(chars = {
            'a',
            'Z',
            '0',
            '9',
            '_'
    })
    void canAppendAcceptsValidCharacters(char validChar) {
        TokenBuilder builder1 = new StandardIdentifierTokenBuilder();
        TokenBuilder builder2 = new StandardIdentifierTokenBuilder().append('a');

        assertTrue(builder1.canAppend(validChar));
        assertTrue(builder2.canAppend(validChar));
    }

    @ParameterizedTest
    @ValueSource(chars = {
            '+',
            '=',
            '}',
            ' ',
            '\t',
            '\n'
    })
    void canAppendRejectsInvalidCharacters(char invalidChar) {
        TokenBuilder builder1 = new StandardIdentifierTokenBuilder();
        TokenBuilder builder2 = new StandardIdentifierTokenBuilder().append('a');

        assertFalse(builder1.canAppend(invalidChar));
        assertFalse(builder2.canAppend(invalidChar));
    }

    @Test
    void canAppendAcceptsPeriodAtStart() {
        TokenBuilder builder = new StandardIdentifierTokenBuilder();

        assertTrue(builder.canAppend('.'));
    }

    @Test
    void canAppendRejectsPeriodsNotAtStart() {
        TokenBuilder builder = new StandardIdentifierTokenBuilder().append('a');

        assertFalse(builder.canAppend('.'));
    }

    @Test
    void canAppendAcceptsValidSurrogatePairs() {
        // Corresponds to "𝟉".
        char highSurrogate = '\uD835';
        char lowSurrogate = '\uDFC9';

        TokenBuilder builder1 = new StandardIdentifierTokenBuilder();
        TokenBuilder builder2 = new StandardIdentifierTokenBuilder().append(highSurrogate);

        assertTrue(builder1.canAppend(highSurrogate));
        assertTrue(builder2.canAppend(lowSurrogate));
    }

    @Test
    void canAppendRejectsNonLetterSurrogatePairs() {
        // Corresponds to "😺".
        char highSurrogate = '\uD83D';
        char lowSurrogate = '\uDE3A';

        TokenBuilder builder = new StandardIdentifierTokenBuilder().append(highSurrogate);

        assertFalse(builder.canAppend(lowSurrogate));
    }

    @Test
    void canAppendRejectsInvalidSurrogatePairs() {
        char highSurrogate = '\uD835';
        char lowSurrogate = '\u0041';

        TokenBuilder builder = new StandardIdentifierTokenBuilder().append(highSurrogate);

        assertFalse(builder.canAppend(lowSurrogate));
    }

    @Test
    void canAppendAcceptsSurrogatePairStarts() {
        TokenBuilder builder = new StandardIdentifierTokenBuilder();

        assertTrue(builder.canAppend('\uD835'));
    }

    @ParameterizedTest
    @MethodSource("validIdentifiers")
    void isValidStandardIdentifierReturnsTrueForValidIdentifier(String identifier) {
        assertTrue(StandardIdentifierTokenBuilder.isValidStandardIdentifier(identifier));
    }

    @ParameterizedTest
    @MethodSource("invalidIdentifiers")
    void isValidStandardIdentifierReturnsFalseForInvalidIdentifier(String identifier) {
        assertFalse(StandardIdentifierTokenBuilder.isValidStandardIdentifier(identifier));
    }
}