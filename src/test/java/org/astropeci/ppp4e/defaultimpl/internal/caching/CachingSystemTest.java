/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.caching;

import org.astropeci.ppp4e.parsing.CacheHint;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CachingSystemTest {

    class DummyCachingSubSystem<K, V> implements CachingSubSystem<K, V> {
        @Override
        public boolean isCached(K key) {
            throw new UnsupportedOperationException();
        }

        @Override
        public V getOrThrow(K key) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void put(K key, V value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void invalidate(K key) {
            throw new UnsupportedOperationException();
        }

        @Override
        public V access(K key, Supplier<V> supplier) {
            throw new UnsupportedOperationException();
        }
    }

    @Test
    void isCachedDelegatesToSubsystem() {
        AtomicReference<String> receivedKey = new AtomicReference<>();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                return new DummyCachingSubSystem<>() {
                    @Override
                    public boolean isCached(K key) {
                        receivedKey.set((String) key);
                        return true;
                    }
                };
            }
        }, CacheHint.SPARSE_CACHING);

        assertTrue(cache.isCached("example"));
        assertEquals(receivedKey.get(), "example");
    }

    @Test
    void getOrThrowDelegatesToSubsystem() {
        AtomicReference<String> receivedKey = new AtomicReference<>();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                return new DummyCachingSubSystem<>() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public V getOrThrow(K key) {
                        receivedKey.set((String) key);
                        return (V) Integer.valueOf(42);
                    }
                };
            }
        }, CacheHint.SPARSE_CACHING);

        assertEquals(cache.getOrThrow("number"), Integer.valueOf(42));
        assertEquals(receivedKey.get(), "number");
    }

    @Test
    void putDelegatesToSubsystem() {
        AtomicReference<String> receivedKey = new AtomicReference<>();
        AtomicInteger receivedValue = new AtomicInteger();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                return new DummyCachingSubSystem<>() {
                    @Override
                    public void put(K key, V value) {
                        receivedKey.set((String) key);
                        receivedValue.set((Integer) value);
                    }
                };
            }
        }, CacheHint.SPARSE_CACHING);

        cache.put("number", 42);

        assertEquals(receivedKey.get(), "number");
        assertEquals(receivedValue.get(), 42);
    }

    @Test
    void invalidateDelegatesToSubsystem() {
        AtomicReference<String> receivedKey = new AtomicReference<>();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                return new DummyCachingSubSystem<>() {
                    @Override
                    public void invalidate(K key) {
                        receivedKey.set((String) key);
                    }
                };
            }
        }, CacheHint.SPARSE_CACHING);

        cache.invalidate("example");

        assertEquals(receivedKey.get(), "example");
    }

    @Test
    void accessDelegatesToSubsystem() {
        AtomicReference<String> receivedKey = new AtomicReference<>();
        AtomicReference<Supplier<Integer>> receivedSupplier = new AtomicReference<>();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                return new DummyCachingSubSystem<>() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public V access(K key, Supplier<V> supplier) {
                        receivedKey.set((String) key);
                        receivedSupplier.set((Supplier<Integer>) supplier);
                        return (V) Integer.valueOf(-49);
                    }
                };
            }
        }, CacheHint.SPARSE_CACHING);

        Supplier<Integer> passedSupplier = () -> 42;

        assertEquals(cache.access("number", passedSupplier), Integer.valueOf(-49));
        assertEquals(receivedKey.get(), "number");
        assertEquals(receivedSupplier.get(), passedSupplier);
    }

    @Test
    void subsystemFactoryIsQueriedOnInstantiation() {
        AtomicReference<CacheHint> receivedHint = new AtomicReference<>();

        new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                receivedHint.set(hint);
                return new DummyCachingSubSystem<>();
            }
        }, CacheHint.MAXIMUM_CACHING);

        assertEquals(receivedHint.get(), CacheHint.MAXIMUM_CACHING);
    }

    @Test
    void applyCacheHintQueriesSubsystemFactoryWhenHintChanged() {
        AtomicReference<CacheHint> receivedHint = new AtomicReference<>();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                receivedHint.set(hint);
                return new DummyCachingSubSystem<>();
            }
        }, CacheHint.SPARSE_CACHING);

        assertEquals(receivedHint.get(), CacheHint.SPARSE_CACHING);
        cache.applyCacheHint(CacheHint.MAXIMUM_CACHING);
        assertEquals(receivedHint.get(), CacheHint.MAXIMUM_CACHING);
    }

    @Test
    void applyCacheHintDoesNotQuerySubsystemFactoryWhenHintNotChanged() {
        AtomicInteger timesCalled = new AtomicInteger();

        CachingSystem<String, Integer> cache = new CachingSystem<>(new CachingSubSystemFactory() {
            @Override
            public <K, V> CachingSubSystem<K, V> createCachingSubSystem(CacheHint hint) {
                assertEquals(hint, CacheHint.SPARSE_CACHING);
                timesCalled.getAndIncrement();
                return new DummyCachingSubSystem<>();
            }
        }, CacheHint.SPARSE_CACHING);

        assertEquals(timesCalled.get(), 1);
        cache.applyCacheHint(CacheHint.SPARSE_CACHING);
        assertEquals(timesCalled.get(), 1);
    }
}