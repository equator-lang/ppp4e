/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl.internal.lexing;

import org.astropeci.ppp4e.parsing.lexing.token.Token;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

class AbstractTokenBuilderTest {

    class DummyAbstractTokenBuilder extends AbstractTokenBuilder {

        public DummyAbstractTokenBuilder() {
            super();
        }

        public DummyAbstractTokenBuilder(String content) {
            super(content);
        }

        @Override
        public boolean isValidToken() {
            return true;
        }

        @Override
        public Token toToken() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean canAppend(char c) {
            return true;
        }

        @Override
        protected TokenBuilder getBuilderWithNewContent(String content) {
            TokenBuilder self = this;

            return new DummyAbstractTokenBuilder(content) {
                @Override
                public boolean isValidToken() {
                    return self.isValidToken();
                }

                @Override
                public Token toToken() {
                    return self.toToken();
                }

                @Override
                public boolean canAppend(char c) {
                    return self.canAppend(c);
                }
            };
        }
    }

    @Test
    void isEmptyReturnsTrueWhenEmpty() {
        TokenBuilder builder = new DummyAbstractTokenBuilder();

        assertTrue(builder.isEmpty());
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "abc", "_abc", "42", "_42f", "._42L", ".a_b_c_", "πﬖ"})
    void isEmptyReturnsFalseWhenFilled(String content) {
        TokenBuilder builder = new DummyAbstractTokenBuilder();
        for (char c : content.toCharArray()) {
            builder = builder.append(c);
        }

        assertFalse(builder.isEmpty());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "a", "abc", "_abc", "42", "_42f", "._42L", ".a_b_c_", "πﬖ"})
    void getRawReturnsContent(String content) {
        TokenBuilder builder = new DummyAbstractTokenBuilder();
        for (char c : content.toCharArray()) {
            builder = builder.append(c);
        }

        assertEquals(builder.getRaw(), content);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "42", "abc"})
    void appendDelegatesToGetBuilderWithNewContentWithAppendedContent(String startContent) {
        AtomicReference<String> passedContent = new AtomicReference<>();

        TokenBuilder builder1 = new DummyAbstractTokenBuilder(startContent) {
            @Override
            protected TokenBuilder getBuilderWithNewContent(String content) {
                passedContent.set(content);
                return this;
            }
        };

        builder1.append('!');

        assertEquals(passedContent.get(), startContent + "!");
    }

    @Test
    void appendFailsWhenCanAppendReturnsFalse() {
        TokenBuilder builder = new DummyAbstractTokenBuilder() {
            @Override
            public boolean canAppend(char c) {
                return false;
            }
        };

        assertThrows(IllegalStateException.class, () -> builder.append('a'));
    }
}