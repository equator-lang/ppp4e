/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.parsing.ParsingContext;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultParsingContextTest {

    @Test
    void charSequenceConstructorConstructsWithProvidedSource() {
        ParsingContext context = new DefaultParsingContext("example text");

        assertEquals(context.getRaw(), "example text");
    }

    @Test
    void noArgConstructorConstructsWithEmptySource() {
        ParsingContext context = new DefaultParsingContext();

        assertEquals(context.getRaw(), "");
    }

    @Test
    void getLengthReturnsZeroWithEmptySource() {
        ParsingContext context = new DefaultParsingContext("");

        assertEquals(context.getLength(), 0);
    }

    @Test
    void getLengthReturnsLengthWithFullSource() {
        ParsingContext context = new DefaultParsingContext("example text");

        assertEquals(context.getLength(), "example text".length());
    }

    @Test
    void getRawReturnsSource() {
        ParsingContext context = new DefaultParsingContext("example text");

        assertEquals(context.getRaw(), "example text");
    }

    @Test
    void replaceSourceReplacesCorrectly() {
        ParsingContext context = new DefaultParsingContext("example text");

        context.replaceSource(0, 0, "not ");
        assertEquals(context.getRaw(), "not example text");

        context.replaceSource(4, 8, "");
        assertEquals(context.getRaw(), "not text");

        context.replaceSource(2, 1, " example");
        assertEquals(context.getRaw(), "no example text");
    }
}