/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.ParsingContext;
import org.astropeci.ppp4e.parsing.URLParsingContext;
import org.astropeci.ppp4e.parsing.ast.AST;
import org.astropeci.ppp4e.parsing.lexing.token.Token;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

class DefaultURLParsingContextTest {

    /**
     * A {@link ParsingContext} that defaults its implementations to throwing an {@link UnsupportedOperationException}.
     */
    class DummyParsingContext implements ParsingContext {

        @Override
        public int getLength() {
            throw new UnsupportedOperationException();
        }

        @Override
        public CharSequence getRaw() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void replaceSource(int start, int length, CharSequence replacement) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Optional<AST> parse(PPP4eLogger logger) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Optional<Iterable<Token>> lex(PPP4eLogger logger) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void applyCacheHint(CacheHint hint) {
            throw new UnsupportedOperationException();
        }
    }

    URL getDummyURL() {
        try {
            return new URL("file://the/content/of/this/url/does/not/matter");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void getWrappedReturnsParsingContextPassedToConstructor() {
        ParsingContext wrapped = new DummyParsingContext();
        DefaultURLParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        assertEquals(context.getWrapped(), wrapped);
    }

    @Test
    void getWrappedReturnsDefaultParsingContextWhenNotOverriddenOnInstantiation() {
        DefaultURLParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8);

        assertTrue(context.getWrapped() instanceof DefaultParsingContext);
    }

    @Test
    void resetFromURLDelegatesToReplaceSourceWithAppropriateParameters() throws IOException {
        AtomicInteger receivedStart = new AtomicInteger(-1);
        AtomicInteger receivedLength = new AtomicInteger(-1);
        AtomicReference<CharSequence> receivedReplacement = new AtomicReference<>();

        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public void replaceSource(int start, int length, CharSequence replacement) {
                receivedStart.set(start);
                receivedLength.set(length);
                receivedReplacement.set(replacement);
            }

            @Override
            public int getLength() {
                return 42;
            }
        };

        URLParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped) {
            @Override
            protected CharSequence getContentFromCurrentURL() {
                return "example text";
            }
        };

        context.resetFromURL();

        assertEquals(receivedStart.get(), 0);
        assertEquals(receivedLength.get(), 42);
        assertEquals(receivedReplacement.get(), "example text");
    }

    @Test
    void getURLReturnsURLPassedToConstructor() {
        URL url = getDummyURL();
        URLParsingContext context = new DefaultURLParsingContext(url, StandardCharsets.UTF_8);

        // Test for reference equality, calling the equals method of URL is a bad idea.
        assertSame(context.getURL(), url);
    }

    @Test
    void setURLChangesResultOfGetURL() {
        URL firstURL = getDummyURL();
        URL secondURL = getDummyURL();
        URLParsingContext context = new DefaultURLParsingContext(firstURL, StandardCharsets.UTF_8);
        context.setURL(secondURL);

        // Test for reference equality, calling the equals method of URL is a bad idea.
        assertSame(context.getURL(), secondURL);
    }

    @Test
    void getCharsetReturnsCharsetPassedToConstructor() {
        Charset charset = StandardCharsets.US_ASCII;
        URLParsingContext context = new DefaultURLParsingContext(getDummyURL(), charset);

        assertEquals(context.getCharset(), charset);
    }

    @Test
    void setCharsetChangesResultOfGetCharset() {
        Charset firstCharset = StandardCharsets.ISO_8859_1;
        Charset secondCharset = StandardCharsets.US_ASCII;
        URLParsingContext context = new DefaultURLParsingContext(getDummyURL(), firstCharset);
        context.setCharset(secondCharset);

        assertEquals(context.getCharset(), secondCharset);
    }

    @Test
    void getLengthDelegateToWrappedGetLength() {
        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public int getLength() {
                return 42;
            }
        };

        ParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        assertEquals(context.getLength(), 42);
    }

    @Test
    void getRawDelegatesToWrappedGetRaw() {
        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public CharSequence getRaw() {
                return "example text";
            }
        };

        ParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        assertEquals(context.getRaw(), "example text");
    }

    @Test
    void replaceSourceDelegatesToWrappedReplaceSource() {
        AtomicInteger receivedStart = new AtomicInteger(-1);
        AtomicInteger receivedLength = new AtomicInteger(-1);
        AtomicReference<CharSequence> receivedReplacement = new AtomicReference<>();

        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public void replaceSource(int start, int length, CharSequence replacement) {
                receivedStart.set(start);
                receivedLength.set(length);
                receivedReplacement.set(replacement);
            }
        };

        ParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        int passedStart = 42;
        int passedLength = 1337;
        String passedReplacement = "example text";

        context.replaceSource(passedStart, passedLength, passedReplacement);

        assertEquals(receivedStart.get(), passedStart);
        assertEquals(receivedLength.get(), passedLength);
        assertEquals(receivedReplacement.get(), passedReplacement);
    }

    @Test
    void parseDelegatesToWrappedParse() {
        AtomicReference<PPP4eLogger> receivedLogger = new AtomicReference<>();

        Optional<AST> returnedAST = Optional.of(new AST() {
            @Override
            public String toString() {
                // Don't throw UnsupportedOperationException as external tools may call this method.
                return "MockedASTObject";
            }
        });

        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public Optional<AST> parse(PPP4eLogger logger) {
                receivedLogger.set(logger);
                return returnedAST;
            }
        };

        ParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        PPP4eLogger passedLogger = message -> {
            throw new UnsupportedOperationException();
        };

        assertEquals(context.parse(passedLogger), returnedAST);
        assertEquals(receivedLogger.get(), passedLogger);
    }

    @Test
    void lexDelegatesToWrappedLex() {
        AtomicReference<PPP4eLogger> receivedLogger = new AtomicReference<>();

        Optional<Iterable<Token>> returnedTokens = Optional.of(() -> {
            throw new UnsupportedOperationException();
        });

        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public Optional<Iterable<Token>> lex(PPP4eLogger logger) {
                receivedLogger.set(logger);
                return returnedTokens;
            }
        };

        ParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        PPP4eLogger passedLogger = message -> {
            throw new UnsupportedOperationException();
        };

        assertEquals(context.lex(passedLogger), returnedTokens);
        assertEquals(receivedLogger.get(), passedLogger);
    }

    @Test
    void applyCacheHintDelegatesToWrappedApplyCacheHint() {
        AtomicReference<CacheHint> receivedHint = new AtomicReference<>();

        ParsingContext wrapped = new DummyParsingContext() {
            @Override
            public void applyCacheHint(CacheHint hint) {
                receivedHint.set(hint);
            }
        };

        ParsingContext context = new DefaultURLParsingContext(getDummyURL(), StandardCharsets.UTF_8, wrapped);

        context.applyCacheHint(CacheHint.MAXIMUM_CACHING);

        assertEquals(receivedHint.get(), CacheHint.MAXIMUM_CACHING);
    }
}