/*
 * Copyright (c) Llew Vallis 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.astropeci.ppp4e.defaultimpl;

import org.astropeci.ppp4e.defaultimpl.internal.lexing.IdentifierTokenImpl;
import org.astropeci.ppp4e.logging.PPP4eLogger;
import org.astropeci.ppp4e.logging.ParseMessage;
import org.astropeci.ppp4e.logging.ParseMessageSeverity;
import org.astropeci.ppp4e.parsing.CacheHint;
import org.astropeci.ppp4e.parsing.lexing.token.IdentifierToken;
import org.astropeci.ppp4e.parsing.lexing.token.Token;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class LexingIntegration {

    static Map<String, List<Token>> successCases = Map.ofEntries(

            success(""),

            success("example42", stdIdent("example42")),
            success(".𝟉_𝟇42x𝟁", stdIdent(".𝟉_𝟇42x𝟁")),

            success("._++=_$$%%*", symIdent("._++=_$$%%*")),
            success(".", symIdent(".")),
            success("_", symIdent("_")),
            success(".___", symIdent(".___")),

            success("exam.ple.+", stdIdent("exam"), stdIdent(".ple"), symIdent(".+")),
            success("...", symIdent("."), symIdent("."), symIdent("."))
    );

    static Map<String, List<ParseMessage>> failCases = Map.ofEntries(

            fail("\u0000", error(DefaultImplementationMessages.unexpectedToken.get(
                    Map.of("char", "\u0000", "line", "1", "column", "1"))))
    );

    static Map.Entry<String, List<Token>> success(String program, Token... tokens) {
        return Map.entry(program, Arrays.asList(tokens));
    }

    static IdentifierToken stdIdent(String raw) {
        return new IdentifierTokenImpl(raw, false);
    }

    static IdentifierToken symIdent(String raw) {
        return new IdentifierTokenImpl(raw, true);
    }

    static Stream<Arguments> successCases() {
        return successCases.entrySet().stream().map(entry -> Arguments.of(entry.getKey(), entry.getValue()));
    }

    static Map.Entry<String, List<ParseMessage>> fail(String program, ParseMessage msg1, ParseMessage... msgs) {
        List<ParseMessage> msgList = new ArrayList<>(Arrays.asList(msgs));
        msgList.add(0, msg1);
        return Map.entry(program, msgList);
    }

    static ParseMessage error(String msg) {
        return new ParseMessage(msg, ParseMessageSeverity.ERROR);
    }

    static Stream<Arguments> failCases() {
        return failCases.entrySet().stream().map(entry -> Arguments.of(entry.getKey(), entry.getValue()));
    }

    @ParameterizedTest
    @MethodSource("successCases")
    void lexingReturnsExpectedForSuccessCases(String program, List<Token> expected) {
        DefaultLexer lexer = new DefaultLexer();
        lexer.applyCacheHint(CacheHint.NEVER_CACHE);

        Iterator resultIter = lexer.lex(System.out::println, program).get().iterator();

        for (Token token : expected) {
            assertEquals(resultIter.next(), token);
        }

        assertFalse(resultIter.hasNext());
    }

    @ParameterizedTest
    @MethodSource("failCases")
    void lexingLogsExpectedMessagesForFailCases(String program, List<ParseMessage> expectedMsgs) {
        DefaultLexer lexer = new DefaultLexer();
        lexer.applyCacheHint(CacheHint.NEVER_CACHE);

        List<ParseMessage> resultMsgs = new ArrayList<>();
        PPP4eLogger logger = msg -> {
            System.out.println(msg);
            resultMsgs.add(msg);
        };

        assertFalse(lexer.lex(logger, program).isPresent());
        assertEquals(resultMsgs, expectedMsgs);
    }
}
