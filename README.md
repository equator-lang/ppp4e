# PPP4e

Pluggable Parser Provider for Equator is an extensible tool that can be
used for parsing Equator programs. PPP4e dynamically loads parsers on
the classpath, allowing the user to add parser implementations by
including a jar on the classpath. PPP4e also provides a standard
implementation.